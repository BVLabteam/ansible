
DROP FOREIGN TABLE IF EXISTS epermits.certificat_ref_csv;

CREATE FOREIGN TABLE epermits.certificat_ref_csv (
	id text,
	name text,
	issuer text,
	other_name text,
	base_request text,
	grouping text,
	generation_mode text,
	scope text,
	shared text,
	is_additionnal text,
	predecessors text,
	docs_cheking text,
	technical_control text,
	validation text,
	issuance text,
	invoice text,
	default_price text,
	default_vat text,
	price_read_only text,
	is_validity_mandatory text,
	default_validity_days text,
	hscode text,
	regime text,
	origin_country text,
	origin_exception text,
	destination_country text,
	destination_exception text,
	extended_procedure_code text,
	national_procedure_code text,
	amount text,
	signe text,
	enabled text
) SERVER ref_data 
OPTIONS ( filename '/appli/epermits/database/data/engine-authorization-rules-laos.csv', format 'csv', delimiter ';', HEADER 'TRUE' );

-- authorization_ref
INSERT INTO epermits.authorization_ref(id, id_epermits, organisation_id, generation_mode, status, amount, comparison_sign, base_request, name, other_name, regroupement, scope, shared, 
	mandatory, step_signature, step_invoice, step_validation, step_technical_control, step_docs_checking, default_price, default_vat, price_read_only, validity_mandatory, default_validity_days, declaration_type)
	SELECT nextval('epermits.ref_data_seq'), 
		c.id, 
		o.id, 
		case when position('UPLOAD' in c.generation_mode) > 0 then 'UPLOADED' when position('MIX' in c.generation_mode) > 0 then 'MIX' else 'GENERATED' end, 
		position('Vrai' in c.enabled) > 0, 
		cast(c.amount as numeric), 
		c.signe, 
		position('Vrai' in c.base_request) > 0, 
		c.name, 
		c.other_name,
		position('Vrai' in c.grouping) > 0,
		c.scope, 
		position('Vrai' in c.shared) > 0,
		position('Faux' in c.is_additionnal) > 0,
		position('Vrai' in c.issuance) > 0,
		position('Vrai' in c.invoice) > 0,
		position('Vrai' in c.validation) > 0,
		position('Vrai' in c.technical_control) > 0,
		position('Vrai' in c.docs_cheking) > 0,
		cast(c.default_price as numeric),
		cast(c.default_vat as numeric),
		position('Vrai' in c.price_read_only) > 0,
		position('Vrai' in c.is_validity_mandatory) > 0,
		cast(c.default_validity_days as integer),
		c.regime
	FROM epermits.organization o, epermits.certificat_ref_csv c
	WHERE o.acronym=c.issuer 
	GROUP BY c.id, o.id, case when position('UPLOAD' in c.generation_mode) > 0 then 'UPLOADED' when position('MIX' in c.generation_mode) > 0 then 'MIX' else 'GENERATED' end, 
		position('Vrai' in c.enabled) > 0, cast(c.amount as numeric), c.signe, position('Vrai' in c.base_request) > 0, c.name, c.other_name, c.scope, c.grouping,
		c.shared, c.is_additionnal,	c.issuance, c.invoice, c.validation, c.technical_control, c.docs_cheking, c.default_price, c.default_vat, c.price_read_only, c.is_validity_mandatory, c.default_validity_days, c.regime;

-- authorization_ref_customs_regim
INSERT INTO epermits.authorization_ref_customs_regim(authorization_ref_id, customs_regim_id) 
	SELECT a.id, d.id 
	FROM epermits.authorization_ref a, epermits.certificat_ref_csv c, epermits.customs_regim_ref d 
	WHERE a.id_epermits=c.id AND (position(d.code in c.regime) > 0 or c.regime='*')
	GROUP BY a.id, d.id, a.name, a.scope, a.organisation_id, a.declaration_type;

-- authorization_ref_tariff_book
INSERT INTO epermits.authorization_ref_tariff_book(authorization_id, tariff_book_id) 
	SELECT a.id, t.id 
	FROM epermits.authorization_ref a, epermits.certificat_ref_csv c, epermits.tariff_book_ref t 
	WHERE a.id_epermits=c.id AND (position(c.hscode in t.hs_code)=1 or c.hscode='*')
	GROUP BY a.id, t.id, a.name, a.scope, a.organisation_id, a.declaration_type;

-- authorization_ref_origin
INSERT INTO  epermits.authorization_ref_origin (authorization_ref_id, country_id) 
	SELECT ar.id, co.id 
	FROM epermits.authorization_ref ar, epermits.country_ref co, epermits.certificat_ref_csv cr 
	WHERE ar.id_epermits=cr.id AND position(co.code_court in cr.origin_country) > 0 AND cr.origin_country <> '*'
	GROUP BY ar.id, co.id, ar.name, ar.scope, ar.organisation_id, ar.declaration_type;

	
-- authorization_ref_origin_exception 
INSERT INTO epermits.authorization_ref_origin_exception (authorization_ref_id, country_id) 
	SELECT ar.id, co.id 
	FROM epermits.authorization_ref ar, epermits.country_ref co, epermits.certificat_ref_csv cr
	WHERE ar.id_epermits=cr.id AND position(co.code_court in cr.origin_exception) > 0 AND cr.origin_exception <> ''
	GROUP BY ar.id, co.id, ar.name, ar.scope, ar.organisation_id, ar.declaration_type;

	
-- authorization_ref_destination
INSERT INTO epermits.authorization_ref_destination(authorization_ref_id, country_id) 
	SELECT ar.id, co.id 
	FROM epermits.authorization_ref ar, epermits.country_ref co, epermits.certificat_ref_csv cr 
	WHERE ar.id_epermits=cr.id AND position(co.code_court in cr.destination_country) > 0 AND cr.destination_country <> '*'
	GROUP BY ar.id, co.id, ar.name, ar.scope, ar.organisation_id, ar.declaration_type;

-- authorization_ref_destination_exception
INSERT INTO epermits.authorization_ref_destination_exception(authorization_ref_id, country_id) 
	SELECT ar.id, co.id 
	FROM epermits.authorization_ref ar, epermits.country_ref co, epermits.certificat_ref_csv cr 
	WHERE ar.id_epermits=cr.id AND position(co.code_court in cr.destination_exception) > 0 AND cr.destination_exception <> ''
	GROUP BY ar.id, co.id, ar.name, ar.scope, ar.organisation_id, ar.declaration_type;

DROP FOREIGN TABLE IF EXISTS epermits.certificat_ref_csv;