BEGIN;

DELETE FROM epermits.role_permission;
DELETE FROM epermits.permission_right;
DELETE FROM epermits.resource;
DELETE FROM epermits.operation;
ALTER TABLE epermits.permission_right DROP COLUMN IF EXISTS organization_type;


/***************************************************************************************************************************************************************/
/***************************************************************************** OPERATIONS **********************************************************************/
/***************************************************************************************************************************************************************/

INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'READ', 'READ';
INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'WRITE', 'WRITE';
INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'DELETE', 'DELETE';
INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'UPDATE', 'UPDATE';
INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'DOWNLOAD', 'DOWNLOAD';
INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'UPLOAD', 'UPLOAD';
INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'PRINT', 'PRINT';
INSERT INTO epermits.operation (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'ALL', 'ALL Operations';


/***************************************************************************************************************************************************************/
/***************************************************************************** RESOURCES ***********************************************************************/
/***************************************************************************************************************************************************************/

-- Shipments

INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests', 'Menu item : Vos demandes';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.users', 'Tableau utilisateurs';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.organizations', 'Tableau organisations';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-new-request', 'Menu item : Nouvelle demande';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.display', 'Request details ==> vos demandes : Ecran detail de la demande';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.document', 'Request details ==> vos demandes : upload documents';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.authorization', 'Request details ==> vos demandes : autorisation';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-download-epermit', 'Request details ==> vos demandes : download epermit';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.product', 'Request details ==> vos demandes : product';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-cancel-epermit', 'Request details ==> vos demandes : Selection de scan (volet droit)';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-download-bundle-documentary', 'Request details ==> vos demandes : Liasse documentaire';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-download-BUP', 'Request details ==> vos demandes : Bouton telechargement BUP';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details', 'Icône "oeil"/"stylo" pour consultation/modification du ePermit';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.header', 'Header autorisations';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.general', 'Onglet général';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.internal-ref', 'Champs de saisie "Référence interne"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.compliance', 'Liste déroulante "Conformité"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.change-validated-step', 'Liste déroulante "Modifier une étape déjà validée"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.to-be-checked', 'Rubrique "Vérification documentaire"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.tech-checking', 'Rubrique "Expertise technique"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.to-validate', 'Rubrique "Validation"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.to-sign', 'Rubrique "Emission"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.ministry-comments', 'Onglet "Commentaires"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.billing', 'Onglet "Facturation"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.comments', 'Icône "message"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.user.view-edit', 'Icône "oeil"/"stylo" pour consultation/modification d''un utilisateur';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.user.edit.status', 'Toggle Statut';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.user.edit.auto.pdf.download', 'Téléchargement automatique du PDF';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.user.add', 'Bouton Ajouter un utilisateur pour créer un utilisateur';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.users.download', 'Bouton Télécharger au format CSV';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.organization.view-edit', 'Icône "oeil"/"stylo" pour consultation/modification d''une organisation';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.organization.edit.status', 'Toggle Statut';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.organization.add', 'Bouton Ajouter une organisation pour créer une organisation';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'admin.organizations.download', 'Bouton Télécharger au format CSV';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.payment', 'Request details ==> vos demandes : payment';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'payment', 'Paiement';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-download-csv', 'Bouton "Télécharger au format CSV"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.draft-tab', 'Consulter Expéditions en cours/brouillons';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.stakeholders', 'Bloc "Déclarant"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-download-all-docs', 'Bouton "Tout télécharger"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'permit-details.btn-print-preview', 'Bouton d''impression de l''autorisation préremplie';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'requests.details.btn-upload-epermit', 'Bouton upload de l''autorisation scannée';

-- Licences

INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences', 'Menu licences';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.btn-download-csv', 'Bouton "Télécharger au format CSV"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.display', 'Visualisation de la demande sur clic';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.btn-new-request', 'Nouvelle demande';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.stakeholders', 'Bloc "Déclarant"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.document', 'Bloc "Documents"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.btn-download-all-docs', 'Bouton "Tout télécharger"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.authorization', 'Bloc "Licences" ';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.btn-download-epermit', 'Icône "épingle" pour téléchargement de la licence';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.product', 'Bloc des onglets (produits, additionnal info, etc.)';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.btn-cancel-epermit', 'Bouton "Demande d''annulation"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details', 'Icône "oeil"/"stylo" pour consultation/modification de la licence';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.header', 'Header Licence';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.general', 'Onglet général';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.internal-ref', 'Champs de saisie "Référence interne"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.compliance', 'Liste déroulante "Conformité"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.validity-period', 'Champs de saisie "Durée de validité"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.btn-print-preview', 'Bouton d''impression de la licence préremplie';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences.details.btn-upload-epermit', 'Bouton upload de la licence scannée';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.to-be-checked', 'Rubrique "Vérification documentaire"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.tech-checking', 'Rubrique "Expertise technique"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.to-validate', 'Rubrique "Validation"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.to-sign', 'Rubrique "Emission"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.ministry-comments', 'Onglet "Commentaires"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.billing', 'Onglet "Facturation"';
INSERT INTO epermits.resource (id, name, description) SELECT nextval('epermits.ref_data_seq'), 'licences-details.comments', 'Icône "message"';


/***************************************************************************************************************************************************************/
/******************************************************************************* ROLES *************************************************************************/
/***************************************************************************************************************************************************************/

/*
INSERT INTO epermits.role (id, name, created_by, updated_by, created_on, updated_on, description, rank) 
	SELECT nextval('epermits.ref_data_seq'), 'HEAD_OF_DIVISION', 'BV_LOCAL_ADMIN', 'BV_LOCAL_ADMIN', '2015-11-18', '2015-11-18', 'HEAD_OF_DIVISION role', 300;
INSERT INTO epermits.role (id, name, created_by, updated_by, created_on, updated_on, description, rank) 
	SELECT nextval('epermits.ref_data_seq'), 'INVOICING_CASHIER', 'BV_LOCAL_ADMIN', 'BV_LOCAL_ADMIN', '2015-11-18', '2015-11-18', 'INVOICING_CASHIER role', 300;

UPDATE epermits.role SET name = 'TECHNICAL_OFFICER', updated_on = '2015-11-18', description ='TECHNICAL_OFFICER role', rank = 300 WHERE name = 'MINISTRY_OFFICER';
UPDATE epermits.role SET name = 'DG_DEPARTMENT', updated_on = '2015-11-18', description ='DG_DEPARTMENT role', rank = 200 WHERE name = 'MINISTRY_DIRECTOR';

DELETE FROM epermits.role WHERE name IN ('MINISTRY_OFFICER_1', 'MINISTRY_OFFICER_2', 'MINISTRY_OFFICER_3', 'MINISTRY_OFFICER_4');
*/


/***************************************************************************************************************************************************************/
/************************************************************************* Permission Right ********************************************************************/
/***************************************************************************************************************************************************************/

-- Shipments

INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'payment';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'payment';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.users';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.users';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.organizations';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.organizations';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-download-csv';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-download-csv';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.draft-tab';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.draft-tab';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-new-request';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-new-request';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.display';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.display';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.stakeholders';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.stakeholders';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.document';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.document';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-download-all-docs';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-download-all-docs';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.authorization';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.authorization';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-download-epermit';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-download-epermit';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.product';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.product';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-cancel-epermit';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-cancel-epermit';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-download-bundle-documentary';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-download-bundle-documentary';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-download-BUP';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-download-BUP';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.header';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.general';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.general';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.internal-ref';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.internal-ref';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.compliance';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.compliance';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.btn-print-preview';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.btn-print-preview';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.btn-upload-epermit';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.btn-upload-epermit';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.change-validated-step';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.change-validated-step';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.to-be-checked';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.to-be-checked';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.tech-checking';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.tech-checking';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.to-validate';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.to-validate';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.to-sign';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.to-sign';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.ministry-comments';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.ministry-comments';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.billing';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.billing';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'permit-details.comments';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'permit-details.comments';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.user.view-edit';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.user.view-edit';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.user.edit.status';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.user.edit.status';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.user.edit.auto.pdf.download';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.user.edit.auto.pdf.download';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.user.add';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.user.add';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.users.download';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.users.download';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.organization.view-edit';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.organization.view-edit';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.organization.edit.status';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.organization.edit.status';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.organization.add';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.organization.add';


INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'admin.organizations.download';
INSERT INTO epermits.permission_right(id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'admin.organizations.download';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'requests.details.payment';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'requests.details.payment';

-- Licences

INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.btn-download-csv';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.btn-download-csv';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.display';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.display';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.btn-new-request';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.btn-new-request';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.stakeholders';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.stakeholders';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.document';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.document';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.btn-download-all-docs';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.btn-download-all-docs';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.authorization';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.authorization';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.btn-download-epermit';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.btn-download-epermit';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.product';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.product';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.btn-cancel-epermit';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.btn-cancel-epermit';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.header';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.general';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.general';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.internal-ref';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.internal-ref';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.compliance';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.compliance';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.validity-period';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.validity-period';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.btn-print-preview';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.btn-print-preview';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences.details.btn-upload-epermit';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences.details.btn-upload-epermit';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.to-be-checked';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.to-be-checked';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.tech-checking';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.tech-checking';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.to-validate';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.to-validate';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.to-sign';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.to-sign';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.ministry-comments';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.ministry-comments';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.billing';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.billing';


INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'WRITE' AND rs.name = 'licences-details.comments';
INSERT INTO epermits.permission_right (id, operation_id, resource_id) 
	SELECT nextval('epermits.ref_data_seq'), op.id, rs.id FROM epermits.operation op, epermits.resource rs WHERE op.name = 'READ' AND rs.name = 'licences-details.comments';



/***************************************************************************************************************************************************************/
/************************************************************************* Role Permission *********************************************************************/
/***************************************************************************************************************************************************************/

-- Shipments

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.draft-tab' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-new-request' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.draft-tab' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-new-request' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.change-validated-step' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.change-validated-step' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'payment' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.payment' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CASHIER';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'payment' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.payment' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CASHIER';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.users' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organizations' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.users.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organizations.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.users' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organizations' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.users.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organizations.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.users' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organizations' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.change-validated-step' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.users.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organizations.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.users' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organizations' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.change-validated-step' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.user.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.user.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.user.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.users.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organization.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organization.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organization.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organizations.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'payment' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.users' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organizations' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.change-validated-step' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.edit.auto.pdf.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.user.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.users.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organization.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'admin.organizations.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'payment' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.users' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organizations' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-bundle-documentary' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-download-BUP' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'requests.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.change-validated-step' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'permit-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.user.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.user.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.user.edit.auto.pdf.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.user.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.users.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organization.view-edit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organization.edit.status' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organization.add' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'admin.organizations.download' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';

-- Licences

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-new-request' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-new-request' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT'; 
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CLEARING_AGENT';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';	
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'TECHNICAL_OFFICER';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'HEAD_OF_DIVISION';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'INVOICING_CASHIER';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'DG_DEPARTMENT';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'CUSTOMS_OFFICER';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_USER_SUPPORT';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_LOCAL_ADMIN';




INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.header' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.general' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'READ' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';

INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-csv' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.display' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.stakeholders' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.document' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-all-docs' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.authorization' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-download-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.product' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-cancel-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.internal-ref' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.compliance' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.validity-period' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.btn-print-preview' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences.details.btn-upload-epermit' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-be-checked' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.tech-checking' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-validate' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.to-sign' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.ministry-comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.billing' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';
INSERT INTO epermits.role_permission (role_id, permission_id) SELECT role.id, pr.id FROM epermits.role role, epermits.permission_right pr, epermits.operation op, epermits.resource r 
	WHERE op.name = 'WRITE' AND r.name = 'licences-details.comments' AND pr.resource_id = r.id  AND pr.operation_id = op.id AND role.name = 'BV_HO_ADMIN';


COMMIT;
