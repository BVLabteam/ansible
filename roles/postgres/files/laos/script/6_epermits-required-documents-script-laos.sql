
DROP FOREIGN TABLE IF EXISTS epermits.required_docs_ref_csv;

CREATE FOREIGN TABLE epermits.required_docs_ref_csv (
	name text,
	other_name text,
	id text,
	enabled text,
	domain text,
	scope text,
	visibility text,
	is_mandatory text,
	declaration_type text,
	base_request text,
	hscode text,
	origine text,
	origine_exception text,
	destination text,
	destination_exception text,
	extended_procedure_code text,
	national_procedure_code text,
	montant text,
	signe text,
	supplementary text
) SERVER ref_data 
OPTIONS ( filename '/appli/epermits/database/data/engine-document-rules-laos.csv', format 'csv', delimiter ';', HEADER 'TRUE' );

INSERT INTO epermits.document_setup(id, status, base_request, name, other_name, document_id, scope, domain, amount, comparison_sign, declaration_type, mandatory, supplementary_fields)
    SELECT nextval('epermits.ref_data_seq'),
    	position('Active' in d.enabled) > 0,
    	position('TRUE' in d.base_request) > 0,
    	d.name,
    	d.other_name,
    	d.id,
    	d.scope, 
    	d.domain, 
    	cast(d.montant as numeric), 
    	d.signe,
    	d.declaration_type, 
    	position('TRUE' in d.is_mandatory) > 0, 
    	position('TRUE' in d.supplementary) > 0
    FROM epermits.required_docs_ref_csv d
    GROUP BY position('Active' in d.enabled) > 0, position('TRUE' in d.base_request) > 0, d.name, d.other_name, d.id, d.scope, d.domain, d.montant, d.signe, d.declaration_type, position('TRUE' in d.is_mandatory) > 0, position('TRUE' in d.supplementary) > 0;


INSERT INTO epermits.document_setup_customs_regim(document_setup_id, customs_regim_id)
    select distinct d.id, c.id 
    from epermits.document_setup d, epermits.customs_regim_ref c, epermits.required_docs_ref_csv rd 
    where d.name=rd.name 
    	and d.scope=rd.scope 
    	and d.declaration_type=rd.declaration_type 
    	and (position(c.code in rd.declaration_type) > 0 or rd.declaration_type = '*')
   group by d.id, c.id, position('Active' in rd.enabled) > 0, position('TRUE' in rd.base_request) > 0, rd.name, rd.scope, rd.domain, rd.montant, rd.signe, rd.declaration_type, position('TRUE' in rd.is_mandatory) > 0, position('TRUE' in rd.supplementary) > 0;



INSERT INTO epermits.document_setup_tariff_book(document_setup_id, tariff_book_id)
    select d.id, t.id from epermits.document_setup d, epermits.tariff_book_ref t, epermits.required_docs_ref_csv rd 
    where d.name=rd.name 
    	and d.scope=rd.scope 
    	and d.declaration_type=rd.declaration_type 
    	and (position(rd.hscode in t.hs_code)=1 or rd.hscode='*')
   group by d.id, t.id, position('Active' in rd.enabled) > 0, position('TRUE' in rd.base_request) > 0, rd.name, rd.scope, rd.domain, rd.montant, rd.signe, rd.declaration_type, position('TRUE' in rd.is_mandatory) > 0, position('TRUE' in rd.supplementary) > 0; 



INSERT INTO epermits.document_setup_origin(document_setup_id, country_id) 
	select dr.id, co.id from epermits.document_setup dr, epermits.country_ref co, epermits.required_docs_ref_csv cr 
	where dr.name=cr.name and dr.scope=cr.scope 
		and dr.declaration_type=cr.declaration_type 
		and ((position(co.code_court in cr.origine) > 0 
		and cr.origine <> '' and cr.origine is not null) or cr.origine='*') 
   group by dr.id, co.id, position('Active' in cr.enabled) > 0, position('TRUE' in cr.base_request) > 0, cr.name, cr.scope, cr.domain, cr.montant, cr.signe, cr.declaration_type, position('TRUE' in cr.is_mandatory) > 0, position('TRUE' in cr.supplementary) > 0;



INSERT INTO epermits.document_setup_origin_exception (document_setup_id, country_id) 
	select dr.id, co.id 
	from epermits.document_setup dr, epermits.country_ref co, epermits.required_docs_ref_csv cr 
	where dr.name=cr.name 
		and dr.scope=cr.scope 
		and dr.declaration_type=cr.declaration_type 
		and position(co.code_court in cr.origine_exception) > 0 and cr.origine_exception <> '' 
		and cr.origine_exception is not null  
   group by dr.id, co.id, position('Active' in cr.enabled) > 0, position('TRUE' in cr.base_request) > 0, cr.name, cr.scope, cr.domain, cr.montant, cr.signe, cr.declaration_type, position('TRUE' in cr.is_mandatory) > 0, position('TRUE' in cr.supplementary) > 0;


INSERT INTO epermits.document_setup_destination(document_setup_id, country_id) 
	select dr.id, co.id 
	from epermits.document_setup dr, epermits.country_ref co, epermits.required_docs_ref_csv cr 
	where dr.name=cr.name 
	and dr.scope=cr.scope 
	and dr.declaration_type=cr.declaration_type 
	and ((position(co.code_court in cr.destination) > 0 and cr.destination <> '' and cr.destination is not null) or cr.destination='*')  
   group by dr.id, co.id, position('Active' in cr.enabled) > 0, position('TRUE' in cr.base_request) > 0, cr.name, cr.scope, cr.domain, cr.montant, cr.signe, cr.declaration_type, position('TRUE' in cr.is_mandatory) > 0, position('TRUE' in cr.supplementary) > 0;


INSERT INTO epermits.document_setup_destination_exception(document_setup_id, country_id) 
	select dr.id, co.id 
	from epermits.document_setup dr, epermits.country_ref co, epermits.required_docs_ref_csv cr  
	where dr.name=cr.name 
		and dr.scope=cr.scope 
		and dr.declaration_type=cr.declaration_type 
		and position(co.code_court in cr.destination_exception) > 0 
		and cr.destination_exception <> '' 
		and cr.destination_exception is not null  
   	group by dr.id, co.id, position('Active' in cr.enabled) > 0, position('TRUE' in cr.base_request) > 0, cr.name, cr.scope, cr.domain, cr.montant, cr.signe, cr.declaration_type, position('TRUE' in cr.is_mandatory) > 0, position('TRUE' in cr.supplementary) > 0;


--
INSERT INTO epermits.document_setup_extended_code(document_setup_id, extended_procedure_code_id) 
	select ds.id, co.id 
	from epermits.document_setup ds, epermits.extended_procedure_code_ref co, epermits.required_docs_ref_csv csv  
	where ds.name=csv.name 
		and ds.scope=csv.scope 
		and ds.declaration_type=csv.declaration_type
		and (position(co.code in csv.extended_procedure_code) > 0 or csv.extended_procedure_code='*')
		and co.custom_regime_id in (select custom_regime_id from epermits.document_setup_customs_regim where document_setup_id = ds.id)
   	group by ds.id, co.id, position('Active' in csv.enabled) > 0, position('TRUE' in csv.base_request) > 0, csv.name, csv.scope, csv.domain, csv.montant, csv.signe, csv.declaration_type, position('TRUE' in csv.is_mandatory) > 0, position('TRUE' in csv.supplementary) > 0;


-- 
INSERT INTO epermits.document_setup_national_code(document_setup_id, national_procedure_code_id) 
	select ds.id, co.id 
	from epermits.document_setup ds, epermits.national_procedure_code_ref co, epermits.required_docs_ref_csv csv  
	where ds.name=csv.name 
		and ds.scope=csv.scope 
		and ds.declaration_type=csv.declaration_type
		and (position(co.code in csv.national_procedure_code) > 0 or csv.national_procedure_code='*')
		and co.extended_procedure_code_id in (select extended_procedure_code_id from epermits.document_setup_extended_code where document_setup_id = ds.id)
   	group by ds.id, co.id, position('Active' in csv.enabled) > 0, position('TRUE' in csv.base_request) > 0, csv.name, csv.scope, csv.domain, csv.montant, csv.signe, csv.declaration_type, position('TRUE' in csv.is_mandatory) > 0, position('TRUE' in csv.supplementary) > 0;

DROP FOREIGN TABLE IF EXISTS epermits.required_docs_ref_csv;