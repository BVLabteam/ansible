#!/bin/sh

# Repertoire java sur la machine cible
if [[ $JAVA_HOME -eq "" ]]; then
	JAVA_HOME=/usr/java/default	 
fi

# Version de l'executable - sera mise à jour automatiquement à chaque build maven
EXEC_VERSION=1.5.0-SNAPSHOT

# Nom de l'executable
EXEC_NAME=epermits-batch-organization

# Nom complet du jar
JAR_NAME=${EXEC_NAME}-${EXEC_VERSION}-shaded.jar

# Repertoire des executables
EXEC_DIR=bin

# Exec JAVA
JAVA_EXEC=${JAVA_HOME}/bin/java


# Repertoires à inclure dans le classpath
CP=./config:./spring:${EXEC_DIR}/${JAR_NAME}


# Class main
MAIN_CLASS=org.springframework.batch.core.launch.support.CommandLineJobRunner

# Fichier du context Spring
SPRING_CONTEXT=spring/appContext-org-batch.xml

# Nom du job à lancer
JOB_NAME=orgImpoterJob

# Fichier de conf
PROPERTIES_PATH=./config/batch-config.properties
PROPERTIES_PARAM=config.properties.path=${PROPERTIES_PATH}

# Chemin vers le fichier de logs
LOGS_PATH=/appli/epermits/database/logs/batch-oraganization.log
LOGS_PARAM=epermits.batch.logging.file=${LOGS_PATH}

#Lancement du batch
${JAVA_EXEC} -D${PROPERTIES_PARAM} -D${LOGS_PARAM} -classpath ${CP} ${MAIN_CLASS} ${SPRING_CONTEXT} ${JOB_NAME}

return_code=$?

exit $return_code