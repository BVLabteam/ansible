--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.4.1
-- Started on 2015-11-28 16:15:30

CREATE SCHEMA IF NOT EXISTS epermits;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;


DROP SERVER IF EXISTS ref_data CASCADE;

DROP EXTENSION IF EXISTS file_fdw;

CREATE EXTENSION file_fdw SCHEMA epermits;

CREATE SERVER ref_data FOREIGN DATA WRAPPER file_fdw;


--
-- TOC entry 7 (class 2615 OID 74424)
-- Name: epermits; Type: SCHEMA; Schema: -; Owner: epermits
--




ALTER SCHEMA epermits OWNER TO epermits;

SET search_path = epermits, pg_catalog;

--
-- TOC entry 177 (class 1259 OID 74438)
-- Name: alfresco_reference_seq; Type: SEQUENCE; Schema: epermits; Owner: epermits
--

CREATE SEQUENCE alfresco_reference_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;


ALTER TABLE alfresco_reference_seq OWNER TO epermits;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 179 (class 1259 OID 74447)
-- Name: attach_doc_org_join; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE attach_doc_org_join (
    organization_id bigint NOT NULL,
    attachement_document_id bigint NOT NULL
);


ALTER TABLE attach_doc_org_join OWNER TO epermits;

--
-- TOC entry 180 (class 1259 OID 74452)
-- Name: attachement_document_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE attachement_document_ref (
    id bigint NOT NULL,
    tariff_book_id bigint NOT NULL,
    mandatory boolean,
    name character varying(100)
);


ALTER TABLE attachement_document_ref OWNER TO epermits;

--
-- TOC entry 181 (class 1259 OID 74458)
-- Name: authorization; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE "authorization" (
    id bigint NOT NULL,
    internal_reference character varying(100),
    edm_id character varying(50),
    authorization_ref_id bigint NOT NULL,
    request_id bigint,
    status character varying(20),
    conformity boolean,
    expertise text,
    observations text,
    invoice_reference character varying(50),
    invoice_description text,
    pre_tax_amount numeric(16,2),
    vat numeric(16,2),
    bv_reference character varying(11),
    total_amount numeric(16,2),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    created_by character varying(100),
    updated_by character varying(100),
    vehicle_form_id bigint,
    comment text,
    filename character varying(128),
    process_id character varying(32),
    nb_steps integer,
    invoice_status character varying(32),
    permit_valid boolean,
    expert_report_valid boolean,
    doc_status boolean,
    no_invoice boolean,
    invoice_validated boolean,
    validated_on timestamp(6) with time zone,
    tech_checking_on timestamp(6) with time zone,
    checked_on timestamp(6) with time zone,
    signed_on timestamp(6) with time zone,
    signed_by character varying(256),
    validated_by character varying(256),
    tech_checking_by character varying(256),
    checked_by character varying(256),
    license_request_id bigint,
    cancelled_on timestamp(6) with time zone,
    cancelled_by character varying(100),
    validity_days integer
);


ALTER TABLE "authorization" OWNER TO epermits;

--
-- TOC entry 182 (class 1259 OID 74466)
-- Name: authorization_product_join; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_product_join (
    authorization_id bigint NOT NULL,
    product_id bigint NOT NULL
);


ALTER TABLE authorization_product_join OWNER TO epermits;

--
-- TOC entry 183 (class 1259 OID 74471)
-- Name: authorization_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref (
    id bigint NOT NULL,
    organisation_id bigint,
    generation_mode character varying(20),
    status boolean,
    amount numeric(16,3),
    comparison_sign character varying(2),
    base_request boolean,
    name character varying(100),
    regroupement boolean,
    scope character varying(32),
    template character varying(64),
    declaration_type character varying(128),
    shared boolean,
    step_suspend boolean,
    step_invoice boolean,
    step_signature boolean,
    step_validation boolean,
    step_technical_control boolean,
    step_docs_checking boolean,
    mandatory boolean,
    id_epermits character varying(32),
    other_name character varying(200),
    default_price numeric(16,3),
    price_read_only boolean,
    validity_mandatory boolean,
    default_validity_days integer,
    default_vat numeric(16,3)
);


ALTER TABLE authorization_ref OWNER TO epermits;

--
-- TOC entry 184 (class 1259 OID 74477)
-- Name: authorization_ref_customs_regim; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_customs_regim (
    authorization_ref_id bigint NOT NULL,
    customs_regim_id bigint NOT NULL
);


ALTER TABLE authorization_ref_customs_regim OWNER TO epermits;

--
-- TOC entry 185 (class 1259 OID 74482)
-- Name: authorization_ref_destination; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_destination (
    authorization_ref_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE authorization_ref_destination OWNER TO epermits;

--
-- TOC entry 186 (class 1259 OID 74485)
-- Name: authorization_ref_destination_exception; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_destination_exception (
    authorization_ref_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE authorization_ref_destination_exception OWNER TO epermits;

--
-- TOC entry 187 (class 1259 OID 74488)
-- Name: authorization_ref_extended_code; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_extended_code (
    authorization_ref_id bigint,
    extended_procedure_code_id bigint
);


ALTER TABLE authorization_ref_extended_code OWNER TO epermits;

--
-- TOC entry 188 (class 1259 OID 74491)
-- Name: authorization_ref_national_code; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_national_code (
    authorization_ref_id bigint,
    national_procedure_code_id bigint
);


ALTER TABLE authorization_ref_national_code OWNER TO epermits;

--
-- TOC entry 189 (class 1259 OID 74494)
-- Name: authorization_ref_origin; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_origin (
    authorization_ref_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE authorization_ref_origin OWNER TO epermits;

--
-- TOC entry 190 (class 1259 OID 74497)
-- Name: authorization_ref_origin_exception; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_origin_exception (
    authorization_ref_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE authorization_ref_origin_exception OWNER TO epermits;

--
-- TOC entry 261 (class 1259 OID 77332)
-- Name: authorization_ref_predecessor; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_predecessor (
    predecessor_id bigint NOT NULL,
    ancestor_id bigint NOT NULL
);


ALTER TABLE authorization_ref_predecessor OWNER TO epermits;

--
-- TOC entry 191 (class 1259 OID 74500)
-- Name: authorization_ref_tariff_book; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE authorization_ref_tariff_book (
    authorization_id bigint,
    tariff_book_id bigint
);


ALTER TABLE authorization_ref_tariff_book OWNER TO epermits;

--
-- TOC entry 192 (class 1259 OID 74506)
-- Name: bank_account; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE bank_account (
    id bigint NOT NULL,
    bank_id bigint,
    economic_operator_id bigint,
    iban character varying(34)
);


ALTER TABLE bank_account OWNER TO epermits;

--
-- TOC entry 193 (class 1259 OID 74511)
-- Name: bank_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE bank_ref (
    id bigint NOT NULL,
    bic character varying(11)
);


ALTER TABLE bank_ref OWNER TO epermits;

--
-- TOC entry 268 (class 1259 OID 78168)
-- Name: bv_invoice; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE bv_invoice (
    id bigint NOT NULL,
    reference character varying(32),
    bup_reference character varying(32),
    request_reference character varying(32),
    vat_amount numeric(16,2),
    pre_tax_amount numeric(16,2),
    name character varying(128),
    description character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    created_by character varying(100),
    updated_by character varying(100)
);


ALTER TABLE bv_invoice OWNER TO epermits;

--
-- TOC entry 176 (class 1259 OID 74436)
-- Name: bv_invoice_seq; Type: SEQUENCE; Schema: epermits; Owner: epermits
--

CREATE SEQUENCE bv_invoice_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;


ALTER TABLE bv_invoice_seq OWNER TO epermits;

--
-- TOC entry 194 (class 1259 OID 74516)
-- Name: commercial_invoice; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE commercial_invoice (
    id bigint NOT NULL,
    request_id bigint,
    currency_id bigint,
    reference character varying(35),
    number_item integer,
    number_package integer,
    uploaded_document_id bigint,
    invoice_date date,
    exw numeric(16,3),
    fob_charges numeric(16,3),
    invoice_incoterm character varying(35),
    total_fob numeric(16,3),
    freight numeric(16,3),
    insurance numeric(16,3),
    other_charges numeric(16,3),
    invoice_amount numeric(16,3),
    status character varying(35),
    comment text,
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    license_request_id bigint
);


ALTER TABLE commercial_invoice OWNER TO epermits;

--
-- TOC entry 195 (class 1259 OID 74527)
-- Name: container; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE container (
    id bigint NOT NULL,
    request_id bigint,
    number character varying(35),
    type character varying(35),
    seal_number character varying(70),
    size character varying(10)
);


ALTER TABLE container OWNER TO epermits;

--
-- TOC entry 267 (class 1259 OID 78060)
-- Name: container_product_join; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE container_product_join (
    container_id bigint NOT NULL,
    product_id bigint NOT NULL
);


ALTER TABLE container_product_join OWNER TO epermits;

--
-- TOC entry 197 (class 1259 OID 74538)
-- Name: country_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE country_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    country_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE country_lang OWNER TO epermits;

--
-- TOC entry 196 (class 1259 OID 74533)
-- Name: country_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE country_ref (
    id bigint NOT NULL,
    code character varying(3),
    code_court character varying(2)
);


ALTER TABLE country_ref OWNER TO epermits;

--
-- TOC entry 199 (class 1259 OID 74551)
-- Name: currency_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE currency_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    currency_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE currency_lang OWNER TO epermits;

--
-- TOC entry 198 (class 1259 OID 74544)
-- Name: currency_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE currency_ref (
    id bigint NOT NULL,
    code character varying(3) NOT NULL
);


ALTER TABLE currency_ref OWNER TO epermits;

--
-- TOC entry 201 (class 1259 OID 74562)
-- Name: customs_office_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE customs_office_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    customs_office_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE customs_office_lang OWNER TO epermits;

--
-- TOC entry 200 (class 1259 OID 74557)
-- Name: customs_office_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE customs_office_ref (
    id bigint NOT NULL,
    code character varying(10)
);


ALTER TABLE customs_office_ref OWNER TO epermits;

--
-- TOC entry 202 (class 1259 OID 74568)
-- Name: customs_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE customs_ref (
    id bigint NOT NULL,
    code character varying(30)
);


ALTER TABLE customs_ref OWNER TO epermits;

--
-- TOC entry 260 (class 1259 OID 76358)
-- Name: customs_regim_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE customs_regim_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    customs_regim_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE customs_regim_lang OWNER TO epermits;

--
-- TOC entry 203 (class 1259 OID 74573)
-- Name: customs_regim_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE customs_regim_ref (
    id bigint NOT NULL,
    regim character varying(8),
    code character varying(4)
);


ALTER TABLE customs_regim_ref OWNER TO epermits;

--
-- TOC entry 3689 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN customs_regim_ref.regim; Type: COMMENT; Schema: epermits; Owner: epermits
--

COMMENT ON COLUMN customs_regim_ref.regim IS 'Valeurs possibles : IMPORT, EXPORT, TRANSIT';


--
-- TOC entry 3690 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN customs_regim_ref.code; Type: COMMENT; Schema: epermits; Owner: epermits
--

COMMENT ON COLUMN customs_regim_ref.code IS '(IM4, EX8...)';


--
-- TOC entry 204 (class 1259 OID 74578)
-- Name: document_setup; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup (
    id bigint NOT NULL,
    status boolean,
    base_request boolean,
    name character varying(128),
    scope character varying(8),
    domain character varying(32),
    amount numeric(16,3),
    comparison_sign character varying(2),
    mandatory boolean,
    declaration_type character varying(128),
    other_name character varying(200),
    document_id character varying(32),
    supplementary_fields boolean
);


ALTER TABLE document_setup OWNER TO epermits;

--
-- TOC entry 205 (class 1259 OID 74583)
-- Name: document_setup_customs_regim; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_customs_regim (
    document_setup_id bigint NOT NULL,
    customs_regim_id bigint NOT NULL
);


ALTER TABLE document_setup_customs_regim OWNER TO epermits;

--
-- TOC entry 206 (class 1259 OID 74588)
-- Name: document_setup_destination; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_destination (
    document_setup_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE document_setup_destination OWNER TO epermits;

--
-- TOC entry 207 (class 1259 OID 74593)
-- Name: document_setup_destination_exception; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_destination_exception (
    document_setup_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE document_setup_destination_exception OWNER TO epermits;

--
-- TOC entry 208 (class 1259 OID 74598)
-- Name: document_setup_extended_code; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_extended_code (
    document_setup_id bigint NOT NULL,
    extended_procedure_code_id bigint NOT NULL
);


ALTER TABLE document_setup_extended_code OWNER TO epermits;

--
-- TOC entry 209 (class 1259 OID 74603)
-- Name: document_setup_national_code; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_national_code (
    document_setup_id bigint NOT NULL,
    national_procedure_code_id bigint NOT NULL
);


ALTER TABLE document_setup_national_code OWNER TO epermits;

--
-- TOC entry 210 (class 1259 OID 74608)
-- Name: document_setup_origin; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_origin (
    document_setup_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE document_setup_origin OWNER TO epermits;

--
-- TOC entry 211 (class 1259 OID 74613)
-- Name: document_setup_origin_exception; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_origin_exception (
    document_setup_id bigint NOT NULL,
    country_id bigint NOT NULL
);


ALTER TABLE document_setup_origin_exception OWNER TO epermits;

--
-- TOC entry 212 (class 1259 OID 74618)
-- Name: document_setup_tariff_book; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_tariff_book (
    document_setup_id bigint,
    tariff_book_id bigint
);


ALTER TABLE document_setup_tariff_book OWNER TO epermits;

--
-- TOC entry 213 (class 1259 OID 74624)
-- Name: document_setup_visibility; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_setup_visibility (
    document_setup_id bigint NOT NULL,
    organization_id bigint NOT NULL
);


ALTER TABLE document_setup_visibility OWNER TO epermits;

--
-- TOC entry 214 (class 1259 OID 74629)
-- Name: document_type_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE document_type_ref (
    id bigint NOT NULL,
    code character varying(6) NOT NULL,
    label character varying(200)
);


ALTER TABLE document_type_ref OWNER TO epermits;

--
-- TOC entry 215 (class 1259 OID 74634)
-- Name: economic_operator; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE economic_operator (
    id bigint NOT NULL,
    legal_form character varying(128),
    trade_register_number character varying(35),
    importer boolean NOT NULL,
    exporter boolean NOT NULL,
    agreement_number character varying(50),
    clearing_agent boolean,
    sydonia_code character varying(17),
    tax_identifier_number character varying(35)
);


ALTER TABLE economic_operator OWNER TO epermits;

--
-- TOC entry 216 (class 1259 OID 74639)
-- Name: economic_operator_document_join; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE economic_operator_document_join (
    economic_operator_id bigint NOT NULL,
    uploaded_document_id bigint NOT NULL
);


ALTER TABLE economic_operator_document_join OWNER TO epermits;

--
-- TOC entry 217 (class 1259 OID 74644)
-- Name: epermit_invoice_line; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE epermit_invoice_line (
    id bigint NOT NULL,
    status character varying(35),
    request_id bigint,
    ogranization_id bigint,
    local_reference character varying(35),
    description character varying(60),
    amount numeric(16,3),
    amount_vat numeric(16,3)
);


ALTER TABLE epermit_invoice_line OWNER TO epermits;

--
-- TOC entry 173 (class 1259 OID 74430)
-- Name: epermits_seq; Type: SEQUENCE; Schema: epermits; Owner: epermits
--

CREATE SEQUENCE epermits_seq
    INCREMENT 100
    MINVALUE 0
    MAXVALUE 9223372036854775807
    START 0
    CACHE 1;


ALTER TABLE epermits_seq OWNER TO epermits;

--
-- TOC entry 218 (class 1259 OID 74649)
-- Name: exchange_rate; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE exchange_rate (
    currency_from_id bigint NOT NULL,
    currency_to_id bigint NOT NULL,
    rate_date date NOT NULL,
    rate numeric(16,5)
);


ALTER TABLE exchange_rate OWNER TO epermits;

--
-- TOC entry 220 (class 1259 OID 74659)
-- Name: extended_procedure_code_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE extended_procedure_code_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    extended_procedure_code_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE extended_procedure_code_lang OWNER TO epermits;

--
-- TOC entry 219 (class 1259 OID 74654)
-- Name: extended_procedure_code_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE extended_procedure_code_ref (
    id bigint NOT NULL,
    custom_regime_id bigint NOT NULL,
    code character varying(10)
);


ALTER TABLE extended_procedure_code_ref OWNER TO epermits;

--
-- TOC entry 175 (class 1259 OID 74434)
-- Name: functional_reference_seq; Type: SEQUENCE; Schema: epermits; Owner: epermits
--

CREATE SEQUENCE functional_reference_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;


ALTER TABLE functional_reference_seq OWNER TO epermits;

--
-- TOC entry 222 (class 1259 OID 74670)
-- Name: goods_location_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE goods_location_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    goods_location_ref_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE goods_location_lang OWNER TO epermits;

--
-- TOC entry 221 (class 1259 OID 74665)
-- Name: goods_location_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE goods_location_ref (
    id bigint NOT NULL,
    code character varying(20)
);


ALTER TABLE goods_location_ref OWNER TO epermits;

--
-- TOC entry 258 (class 1259 OID 76338)
-- Name: http_message; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE http_message (
    id bigint NOT NULL,
    http_code bigint NOT NULL,
    internal_code bigint NOT NULL,
    message_type bigint NOT NULL,
    field character varying(64)
);


ALTER TABLE http_message OWNER TO epermits;

--
-- TOC entry 259 (class 1259 OID 76343)
-- Name: http_message_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE http_message_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    http_message_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE http_message_lang OWNER TO epermits;

--
-- TOC entry 223 (class 1259 OID 74676)
-- Name: insurance_contract; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE insurance_contract (
    id bigint NOT NULL,
    insurance_id bigint,
    economic_operator_id bigint,
    broker_name character varying(70),
    certificate_identifiant character varying(50),
    " certificate_date" date,
    insurance_type character varying(50)
);


ALTER TABLE insurance_contract OWNER TO epermits;

--
-- TOC entry 224 (class 1259 OID 74681)
-- Name: insurance_contract_document_join; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE insurance_contract_document_join (
    insurance_contract_id bigint NOT NULL,
    uploaded_document_id bigint NOT NULL
);


ALTER TABLE insurance_contract_document_join OWNER TO epermits;

--
-- TOC entry 225 (class 1259 OID 74686)
-- Name: insurance_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE insurance_ref (
    id bigint NOT NULL
);


ALTER TABLE insurance_ref OWNER TO epermits;

--
-- TOC entry 178 (class 1259 OID 74440)
-- Name: lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE lang (
    id bigint NOT NULL,
    code character varying(6),
    name character varying(64)
);


ALTER TABLE lang OWNER TO epermits;

--
-- TOC entry 276 (class 1259 OID 78850)
-- Name: license_additional_document; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE license_additional_document (
    id bigint NOT NULL,
    license_request_id bigint NOT NULL,

    document_setup_id bigint,
    supplementary_reference  varchar(100),
    supplementary_date timestamp with time zone,

    CONSTRAINT license_additional_document_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES epermits.document_setup (id)

);


ALTER TABLE license_additional_document OWNER TO epermits;

--
-- TOC entry 272 (class 1259 OID 78787)
-- Name: license_document; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE license_document (
    id bigint NOT NULL,
    edm_id character varying(100),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    created_by character varying(100),
    updated_by character varying(100),
    file_name character varying(200),
    description text
);


ALTER TABLE license_document OWNER TO epermits;

--
-- TOC entry 273 (class 1259 OID 78795)
-- Name: license_invoice_document; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE license_invoice_document (
    id bigint NOT NULL,
    commercial_invoice_id bigint NOT NULL
);


ALTER TABLE license_invoice_document OWNER TO epermits;

--
-- TOC entry 275 (class 1259 OID 78830)
-- Name: license_product_document; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE license_product_document (
    id bigint NOT NULL,
    document_setup_id bigint NOT NULL,
    product_id bigint NOT NULL,
    supplementary_reference character varying(100),
    supplementary_date timestamp with time zone
);


ALTER TABLE license_product_document OWNER TO epermits;

--
-- TOC entry 271 (class 1259 OID 78752)
-- Name: license_request; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE license_request (
    id bigint NOT NULL,
    shipment_info_id bigint,
    consignee_id bigint,
    consignor_id bigint,
    clearing_agency_id bigint NOT NULL,
    member_id bigint NOT NULL,
    creation_date date,
    validation_date timestamp(6) with time zone,
    status character varying(35),
    regim character varying(8),
    declaration_type character varying(4),
    current_step character varying(50),
    bv_request_reference character varying(35),
    edm_reference character varying(35),
    process_id character varying(35),
    
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    created_by character varying(100),
    updated_by character varying(100)
);


ALTER TABLE license_request OWNER TO epermits;

--
-- TOC entry 274 (class 1259 OID 78810)
-- Name: license_request_document; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE license_request_document (
    id bigint NOT NULL,
    license_request_id bigint NOT NULL,
    document_setup_id bigint NOT NULL,
    supplementary_reference character varying(100),
    supplementary_date timestamp with time zone
);


ALTER TABLE license_request_document OWNER TO epermits;

--
-- TOC entry 270 (class 1259 OID 78732)
-- Name: license_shipment_info; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE license_shipment_info (
    id bigint NOT NULL,
    country_supply_id bigint,
    country_export_id bigint,
    country_destination_id bigint,
    purpose character varying(40),
    project_name character varying(100),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    created_by character varying(100),
    updated_by character varying(100)
);


ALTER TABLE license_shipment_info OWNER TO epermits;

--
-- TOC entry 226 (class 1259 OID 74691)
-- Name: member; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE member (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    username character varying(128) NOT NULL,
    password character varying(128) NOT NULL,
    enabled boolean NOT NULL,
    lastname character varying(128),
    firstname character varying(128),
    phone_number character varying(50),
    email character varying(50),
    fax_number character varying(50),
    contact_type character varying(128),
    mobile_number character varying(50),
    preferred_channel character varying(35),
    comment text,
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone
);


ALTER TABLE member OWNER TO epermits;

--
-- TOC entry 262 (class 1259 OID 77456)
-- Name: message; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE message (
    id bigint NOT NULL,
    authorization_id bigint NOT NULL,
    message_type character varying(32) NOT NULL,
    message_date timestamp with time zone NOT NULL,
    comment character varying(255) NOT NULL,
    user_id bigint
);


ALTER TABLE message OWNER TO epermits;

--
-- TOC entry 227 (class 1259 OID 74701)
-- Name: ministry_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE ministry_ref (
    id bigint NOT NULL
);


ALTER TABLE ministry_ref OWNER TO epermits;

--
-- TOC entry 229 (class 1259 OID 74711)
-- Name: national_procedure_code_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE national_procedure_code_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    national_procedure_code_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE national_procedure_code_lang OWNER TO epermits;

--
-- TOC entry 228 (class 1259 OID 74706)
-- Name: national_procedure_code_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE national_procedure_code_ref (
    id bigint NOT NULL,
    code character varying(10),
    extended_procedure_code_id bigint
);


ALTER TABLE national_procedure_code_ref OWNER TO epermits;

--
-- TOC entry 230 (class 1259 OID 74717)
-- Name: operation; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE operation (
    id bigint NOT NULL,
    name character varying(120),
    description text
);


ALTER TABLE operation OWNER TO epermits;

--
-- TOC entry 231 (class 1259 OID 74725)
-- Name: organization; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE organization (
    id bigint NOT NULL,
    name character varying(200),
    other_name character varying(200),
    acronym character varying(50),
    edm_logo_reference character varying(35),
    address character varying(128),
    additional_address character varying(128),
    zip_code character varying(10),
    city character varying(35),
    manager_lastname character varying(50),
    manager_firstname character varying(50),
    phone_number character varying(50),
    email character varying(50),
    fax_number character varying(50),
    mobile_number character varying(50),
    enabled boolean,
    validity_date date,
    organization_parent_id bigint,
    country_id bigint,
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    manager_occupation character varying(200),
    manager_birth_date date,
    manager_nationality bigint
);


ALTER TABLE organization OWNER TO epermits;

--
-- TOC entry 265 (class 1259 OID 77809)
-- Name: packaging_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE packaging_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    packaging_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE packaging_lang OWNER TO epermits;

--
-- TOC entry 264 (class 1259 OID 77802)
-- Name: packaging_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE packaging_ref (
    id bigint NOT NULL,
    code character varying(3) NOT NULL
);


ALTER TABLE packaging_ref OWNER TO epermits;

--
-- TOC entry 266 (class 1259 OID 77927)
-- Name: packaging_ref_csv; Type: FOREIGN TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE FOREIGN TABLE packaging_ref_csv (
    code text,
    libelle_lo text,
    libelle_en text
)
SERVER ref_data
OPTIONS (
    delimiter ';',
    filename '/appli/epermits/database/data/ref-packaging-laos.csv',
    format 'csv',
    header 'TRUE'
);


ALTER FOREIGN TABLE packaging_ref_csv OWNER TO epermits;

--
-- TOC entry 232 (class 1259 OID 74733)
-- Name: parc_vente_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE parc_vente_ref (
    id bigint NOT NULL,
    parc_number integer,
    code character varying(32),
    label character varying(64),
    created_by character varying(64),
    updated_by character varying(64),
    created_on date,
    updated_on date
);


ALTER TABLE parc_vente_ref OWNER TO epermits;

--
-- TOC entry 233 (class 1259 OID 74738)
-- Name: payment; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE payment (
    id bigint NOT NULL,
    bank_id bigint NOT NULL,
    payment_date date NOT NULL,
    request_id bigint NOT NULL,
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone
);


ALTER TABLE payment OWNER TO epermits;

--
-- TOC entry 234 (class 1259 OID 74743)
-- Name: permission_right; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE permission_right (
    id bigint NOT NULL,
    operation_id bigint NOT NULL,
    resource_id bigint NOT NULL
);


ALTER TABLE permission_right OWNER TO epermits;

--
-- TOC entry 236 (class 1259 OID 74753)
-- Name: port_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE port_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    port_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE port_lang OWNER TO epermits;

--
-- TOC entry 235 (class 1259 OID 74748)
-- Name: port_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE port_ref (
    id bigint NOT NULL,
    country_id bigint,
    code character varying(5),
    active boolean
);


ALTER TABLE port_ref OWNER TO epermits;

--
-- TOC entry 257 (class 1259 OID 75811)
-- Name: ports_ref_csv; Type: FOREIGN TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE FOREIGN TABLE ports_ref_csv (
    code text,
    label text,
    long_label text,
    country text NOT NULL,
    active text,
    isps text,
    coun_code text,
    nbcar bigint
)
SERVER ref_data
OPTIONS (
    delimiter ';',
    filename '/appli/epermits/database/data/ref-loading-ports-and-airports-laos.csv',
    format 'csv',
    header 'TRUE'
);


ALTER FOREIGN TABLE ports_ref_csv OWNER TO epermits;

--
-- TOC entry 237 (class 1259 OID 74760)
-- Name: product; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE product (
    id bigint NOT NULL,
    name character varying(256),
    invoice_id bigint NOT NULL,
    tariff_book_id bigint NOT NULL,
    country_id bigint NOT NULL,
    country_destination_id bigint,
    product_incoterm character varying(5),
    manufacturer character varying(70),
    model character varying(35),
    brand character varying(35),
    characteristics character varying(70),
    commercial_unit character varying(10),
    statistical_unit character varying(10),
    commercial_quantity numeric(10,3),
    declared_value numeric(16,3),
    extended_procedure_code_id bigint,
    expiry_date date,
    national_procedure_code_id bigint,
    sequence_number integer,
    statistical_quantity numeric(10,3),
    packing_presentation character varying(100),
    year integer,
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    gross_weight numeric(16,6),
    net_weight numeric(14,3),
    packaging_id bigint,
    vehicle_id bigint
);


ALTER TABLE product OWNER TO epermits;

--
-- TOC entry 3691 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN product.product_incoterm; Type: COMMENT; Schema: epermits; Owner: epermits
--

COMMENT ON COLUMN product.product_incoterm IS 'Valeurs possibles : EXW, FAS, FCA, FOB, CFR, CIF, CIP, CPT, DAP, DAT, DDP';


--
-- TOC entry 277 (class 1259 OID 78865)
-- Name: product_additional_infos; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE product_additional_infos (
    id bigint NOT NULL,
    vin_number character varying(50),
    engine_number character varying(50),
    product_id bigint NOT NULL
);


ALTER TABLE product_additional_infos OWNER TO epermits;

--
-- TOC entry 238 (class 1259 OID 74772)
-- Name: product_document_join; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE product_document_join (
    product_id bigint NOT NULL,
    uploaded_document_id bigint NOT NULL
);


ALTER TABLE product_document_join OWNER TO epermits;

--
-- TOC entry 174 (class 1259 OID 74432)
-- Name: ref_data_seq; Type: SEQUENCE; Schema: epermits; Owner: epermits
--

CREATE SEQUENCE ref_data_seq
    INCREMENT 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    START 0
    CACHE 1;


ALTER TABLE ref_data_seq OWNER TO epermits;

--
-- TOC entry 239 (class 1259 OID 74777)
-- Name: request; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE request (
    id bigint NOT NULL,
    local_reference character varying(50),
    bv_request_reference character varying(35),
    consignee_id bigint,
    consignor_id bigint,
    internal_transport_mode character varying(35),
    bank_account_payment_id bigint,
    bank_account_change_id bigint,
    insurance_contract_id bigint,
    creation_date date,
    validation_date timestamp with time zone,
    expiration_date date,
    first_payement_date date,
    custom_regime_id bigint,
    clearing_agency_id bigint NOT NULL,
    sad_issuing_date date,
    sad_issuing_place character varying(35),
    version character varying(10),
    status character varying(35),
    member_id bigint NOT NULL,
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    created_by character varying(100),
    updated_by character varying(100),
    bpm_reference character varying(50),
    regim character varying(8),
    goods_location_id bigint,
    trader_reference character varying(50),
    number_package integer,
    request_incoterm character varying(50),
    port_incoterm_id bigint,
    current_step character varying(10),
    invoice_comment text,
    bup_edm_id character varying(128),
    scope character varying(32),
    alfresco_reference character varying(35),
    process_id character varying(32),
    bv_invoice_id bigint
);


ALTER TABLE request OWNER TO epermits;

--
-- TOC entry 240 (class 1259 OID 74788)
-- Name: request_transport; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE request_transport (
    id bigint NOT NULL,
    request_id bigint NOT NULL,
    uploaded_document_id bigint,
    loading_port_id bigint,
    transit_port_id bigint,
    unloading_port_id bigint,
    document_type character varying(35),
    document_ref character varying(35),
    document_date timestamp(6) with time zone,
    loading_date timestamp(6) with time zone,
    estimated_arrival_date timestamp(6) with time zone,
    carrier_company character varying(100),
    warehouse_id bigint,
    country_transit_id bigint,
    inland_transport_mode character varying(50),
    country_border_id bigint,
    identity_border_transport character varying(50),
    country_departure_id bigint,
    identity_departure_transport character varying(50),
    transport_doc_number character varying(50),
    manifest_reference character varying(50),
    country_destination_id bigint,
    country_export_id bigint,
    country_supply_id bigint,
    declaration_office_id bigint,
    entry_exit_office_id bigint,
    storage_period integer,
    border_transport_mode character varying(55),
    transport_document boolean,
    loading_unloading_port_id bigint
);


ALTER TABLE request_transport OWNER TO epermits;

--
-- TOC entry 256 (class 1259 OID 75637)
-- Name: required_docs_ref_csv; Type: FOREIGN TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE FOREIGN TABLE required_docs_ref_csv (
    name text,
    id text,
    enabled text,
    domain text,
    scope text,
    visibility text,
    is_mandatory text,
    declaration_type text,
    base_request text,
    hscode text,
    origine text,
    origine_exception text,
    destination text,
    destination_exception text,
    extended_procedure_code text,
    national_procedure_code text,
    montant text,
    signe text
)
SERVER ref_data
OPTIONS (
    delimiter ';',
    filename '/appli/epermits/database/data/engine-document-rules-laos.csv',
    format 'csv',
    header 'TRUE'
);


ALTER FOREIGN TABLE required_docs_ref_csv OWNER TO epermits;

--
-- TOC entry 241 (class 1259 OID 74798)
-- Name: resource; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE resource (
    id bigint NOT NULL,
    name character varying(120),
    description text
);


ALTER TABLE resource OWNER TO epermits;

--
-- TOC entry 242 (class 1259 OID 74806)
-- Name: risk_list; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE risk_list (
    id bigint NOT NULL,
    author_id bigint NOT NULL,
    target_id bigint NOT NULL,
    comment text,
    level character varying(10)
);


ALTER TABLE risk_list OWNER TO epermits;

--
-- TOC entry 243 (class 1259 OID 74814)
-- Name: role; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE role (
    id bigint NOT NULL,
    name character varying(128),
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    description character varying(50),
    rank integer
);


ALTER TABLE role OWNER TO epermits;

--
-- TOC entry 244 (class 1259 OID 74819)
-- Name: role_permission; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE role_permission (
    role_id bigint NOT NULL,
    permission_id bigint NOT NULL,
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone
);


ALTER TABLE role_permission OWNER TO epermits;

--
-- TOC entry 245 (class 1259 OID 74824)
-- Name: single_window_operator_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE single_window_operator_ref (
    id bigint NOT NULL
);


ALTER TABLE single_window_operator_ref OWNER TO epermits;

--
-- TOC entry 246 (class 1259 OID 74829)
-- Name: tariff_book_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE tariff_book_ref (
    id bigint NOT NULL,
    commercial_unit character varying(10),
    statistical_unit character varying(10),
    hs_code character varying(18) NOT NULL,
    status character varying(10),
    start_date date,
    limit_date date,
    prohibited boolean,
    warning text,
    product_year boolean,
    product_statistical_quantity boolean,
    product_expiry_date boolean,
    product_description character varying,
    statistical_quantity boolean
);


ALTER TABLE tariff_book_ref OWNER TO epermits;

--
-- TOC entry 247 (class 1259 OID 74837)
-- Name: transportation_type_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE transportation_type_ref (
    id bigint NOT NULL,
    label character varying(50)
);


ALTER TABLE transportation_type_ref OWNER TO epermits;

--
-- TOC entry 248 (class 1259 OID 74842)
-- Name: uploaded_document; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE uploaded_document (
    id bigint NOT NULL,
    edm_id character varying(50),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    created_by character varying(100),
    updated_by character varying(100),
    request_id bigint,
    document_type character varying(128),
    file_name character varying(200),
    document_domain character varying(50),
    description text,
    document_setup_id bigint,
    vehicle_form_id bigint,
    vehicle_id bigint
);


ALTER TABLE uploaded_document OWNER TO epermits;

--
-- TOC entry 249 (class 1259 OID 74850)
-- Name: user_role; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE user_role (
    member_id bigint NOT NULL,
    role_id bigint NOT NULL,
    created_by character varying(128),
    updated_by character varying(128),
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone
);


ALTER TABLE user_role OWNER TO epermits;

--
-- TOC entry 250 (class 1259 OID 74855)
-- Name: vehicle; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE vehicle (
    id bigint NOT NULL,
    created_by character varying(255),
    created_on timestamp(6) with time zone,
    updated_by character varying(255),
    updated_on timestamp(6) with time zone,
    brand character varying(255),
    color character varying(255),
    engine_number character varying(255),
    kilometrage character varying(255),
    model character varying(255),
    vin character varying(255),
    year integer,
    destination_id bigint,
    exit_border_id bigint,
    vehicle_form_id bigint,
    parc_vente_id bigint,
    bfu_payment_date date,
    declaration_number character varying(128),
    bfu_number character varying(128),
    ship_arrival_date date
);


ALTER TABLE vehicle OWNER TO epermits;

--
-- TOC entry 251 (class 1259 OID 74863)
-- Name: vehicle_form; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE vehicle_form (
    id bigint NOT NULL,
    created_by character varying(255),
    created_on timestamp(6) with time zone,
    updated_by character varying(255),
    updated_on timestamp(6) with time zone,
    buyer_id bigint,
    clearing_agent_id bigint,
    forwarding_agent_id bigint,
    status character varying(35),
    user_id bigint NOT NULL,
    declaration_type character varying(4),
    bv_request_reference character varying(16),
    current_step character varying(5),
    alfresco_reference character varying(35),
    validation_date timestamp with time zone
);


ALTER TABLE vehicle_form OWNER TO epermits;

--
-- TOC entry 252 (class 1259 OID 74871)
-- Name: vehicle_information; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE vehicle_information (
    id bigint NOT NULL,
    first_entry_date date,
    vin_number character varying(70),
    vehicle_type character varying(70),
    autorized_weight character varying(70),
    energy_type character varying(70),
    cylinder_size numeric(10,3),
    power integer,
    kilometrage integer,
    product_id bigint
);


ALTER TABLE vehicle_information OWNER TO epermits;

--
-- TOC entry 253 (class 1259 OID 74876)
-- Name: vehicle_operator; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE vehicle_operator (
    id bigint NOT NULL,
    created_by character varying(255),
    created_on timestamp(6) with time zone,
    updated_by character varying(255),
    updated_on timestamp(6) with time zone,
    email character varying(50),
    fax_number character varying(50),
    mobile_number character varying(50),
    phone_number character varying(50),
    additional_address character varying(255),
    address character varying(255),
    city character varying(255),
    lastname character varying(255),
    firstname character varying(255),
    country_id bigint,
    vehicle_operator_type character varying(32),
    visibility_organization_id bigint
);


ALTER TABLE vehicle_operator OWNER TO epermits;

--
-- TOC entry 269 (class 1259 OID 78650)
-- Name: vehicle_product; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE vehicle_product (
    id bigint NOT NULL,
    type character varying(64),
    color character varying(32),
    engine_number character varying(64),
    frame_number character varying(64),
    engine_power character varying(64),
    cylinders character varying(64),
    usage_duration character varying(64),
    fuel_type character varying(64),
    steering character varying(64),
    number_of_seats integer,
    number_of_wheels integer,
    number_of_suspensions integer,
    height numeric(16,3),
    length numeric(16,3),
    width numeric(16,3),
    production_year integer,
    created_by character varying(255),
    created_on timestamp(6) with time zone,
    updated_by character varying(255),
    updated_on timestamp(6) with time zone
);


ALTER TABLE vehicle_product OWNER TO epermits;

--
-- TOC entry 255 (class 1259 OID 74892)
-- Name: warehouse_lang; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE warehouse_lang (
    id bigint NOT NULL,
    lang_id bigint NOT NULL,
    warehouse_id bigint NOT NULL,
    label character varying(256)
);


ALTER TABLE warehouse_lang OWNER TO epermits;

--
-- TOC entry 254 (class 1259 OID 74884)
-- Name: warehouse_ref; Type: TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE TABLE warehouse_ref (
    id bigint NOT NULL,
    code text
);


ALTER TABLE warehouse_ref OWNER TO epermits;

--
-- TOC entry 263 (class 1259 OID 77683)
-- Name: warehouse_ref_csv; Type: FOREIGN TABLE; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE FOREIGN TABLE warehouse_ref_csv (
    code text,
    libelle text,
    lib_eng text
)
SERVER ref_data
OPTIONS (
    delimiter ';',
    filename '/appli/epermits/database/data/ref-warehouses-laos.csv',
    format 'csv',
    header 'TRUE'
);


ALTER FOREIGN TABLE warehouse_ref_csv OWNER TO epermits;

--
-- TOC entry 3319 (class 2606 OID 74776)
-- Name: Pk_product_document; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY product_document_join
    ADD CONSTRAINT "Pk_product_document" PRIMARY KEY (product_id, uploaded_document_id);


--
-- TOC entry 3293 (class 2606 OID 74710)
-- Name: additional_code_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY national_procedure_code_ref
    ADD CONSTRAINT additional_code_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3190 (class 2606 OID 74451)
-- Name: attach_doc_org_join_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY attach_doc_org_join
    ADD CONSTRAINT attach_doc_org_join_pkey PRIMARY KEY (organization_id, attachement_document_id);


--
-- TOC entry 3200 (class 2606 OID 74475)
-- Name: authorization_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY authorization_ref
    ADD CONSTRAINT authorization_pk PRIMARY KEY (id);


--
-- TOC entry 3195 (class 2606 OID 74465)
-- Name: authorization_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY "authorization"
    ADD CONSTRAINT authorization_pkey PRIMARY KEY (id);


--
-- TOC entry 3197 (class 2606 OID 74470)
-- Name: authorization_product_join_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY authorization_product_join
    ADD CONSTRAINT authorization_product_join_pkey PRIMARY KEY (authorization_id, product_id);


--
-- TOC entry 3367 (class 2606 OID 77336)
-- Name: authorization_ref_predecessor_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY authorization_ref_predecessor
    ADD CONSTRAINT authorization_ref_predecessor_pk PRIMARY KEY (predecessor_id, ancestor_id);


--
-- TOC entry 3207 (class 2606 OID 74510)
-- Name: bank_account_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY bank_account
    ADD CONSTRAINT bank_account_pkey PRIMARY KEY (id);


--
-- TOC entry 3209 (class 2606 OID 74515)
-- Name: bank_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY bank_ref
    ADD CONSTRAINT bank_pk PRIMARY KEY (id);


--
-- TOC entry 3379 (class 2606 OID 78175)
-- Name: bv_invoice_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY bv_invoice
    ADD CONSTRAINT bv_invoice_pk PRIMARY KEY (id);


--
-- TOC entry 3216 (class 2606 OID 74531)
-- Name: container_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY container
    ADD CONSTRAINT container_pk PRIMARY KEY (id);


--
-- TOC entry 3377 (class 2606 OID 78064)
-- Name: container_product_join_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY container_product_join
    ADD CONSTRAINT container_product_join_pkey PRIMARY KEY (container_id, product_id);


--
-- TOC entry 3221 (class 2606 OID 74542)
-- Name: country_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY country_lang
    ADD CONSTRAINT country_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3219 (class 2606 OID 74537)
-- Name: country_ref_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY country_ref
    ADD CONSTRAINT country_ref_pk PRIMARY KEY (id);


--
-- TOC entry 3228 (class 2606 OID 74555)
-- Name: currency_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY currency_lang
    ADD CONSTRAINT currency_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3224 (class 2606 OID 74548)
-- Name: currency_ref_id; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY currency_ref
    ADD CONSTRAINT currency_ref_id PRIMARY KEY (id);


--
-- TOC entry 3233 (class 2606 OID 74566)
-- Name: customs_office_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY customs_office_lang
    ADD CONSTRAINT customs_office_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3231 (class 2606 OID 74561)
-- Name: customs_office_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY customs_office_ref
    ADD CONSTRAINT customs_office_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3236 (class 2606 OID 74572)
-- Name: customs_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY customs_ref
    ADD CONSTRAINT customs_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3365 (class 2606 OID 76362)
-- Name: customs_regim_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY customs_regim_lang
    ADD CONSTRAINT customs_regim_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3238 (class 2606 OID 74577)
-- Name: customs_regim_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY customs_regim_ref
    ADD CONSTRAINT customs_regim_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3240 (class 2606 OID 74582)
-- Name: document_setup_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup
    ADD CONSTRAINT document_setup_pk PRIMARY KEY (id);


--
-- TOC entry 3261 (class 2606 OID 74633)
-- Name: document_type_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_type_ref
    ADD CONSTRAINT document_type_pk PRIMARY KEY (id);


--
-- TOC entry 3265 (class 2606 OID 74643)
-- Name: economic_operator_document_join_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY economic_operator_document_join
    ADD CONSTRAINT economic_operator_document_join_pkey PRIMARY KEY (economic_operator_id, uploaded_document_id);


--
-- TOC entry 3263 (class 2606 OID 74638)
-- Name: economic_operator_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY economic_operator
    ADD CONSTRAINT economic_operator_pk PRIMARY KEY (id);


--
-- TOC entry 3267 (class 2606 OID 74648)
-- Name: epermit_invoice_line_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY epermit_invoice_line
    ADD CONSTRAINT epermit_invoice_line_pkey PRIMARY KEY (id);


--
-- TOC entry 3324 (class 2606 OID 74784)
-- Name: epermit_request_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY request
    ADD CONSTRAINT epermit_request_pk PRIMARY KEY (id);


--
-- TOC entry 3269 (class 2606 OID 74653)
-- Name: exchange_rate_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY exchange_rate
    ADD CONSTRAINT exchange_rate_pkey PRIMARY KEY (currency_from_id, currency_to_id, rate_date);


--
-- TOC entry 3273 (class 2606 OID 74663)
-- Name: extendedprocedurecoderef_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY extended_procedure_code_lang
    ADD CONSTRAINT extendedprocedurecoderef_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3278 (class 2606 OID 74674)
-- Name: goods_location_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY goods_location_lang
    ADD CONSTRAINT goods_location_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3276 (class 2606 OID 74669)
-- Name: goods_location_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY goods_location_ref
    ADD CONSTRAINT goods_location_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3363 (class 2606 OID 76347)
-- Name: http_message_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY http_message_lang
    ADD CONSTRAINT http_message_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3361 (class 2606 OID 76342)
-- Name: http_message_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY http_message
    ADD CONSTRAINT http_message_pk PRIMARY KEY (id);


--
-- TOC entry 3283 (class 2606 OID 74685)
-- Name: insurance_contract_document_join_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY insurance_contract_document_join
    ADD CONSTRAINT insurance_contract_document_join_pkey PRIMARY KEY (insurance_contract_id, uploaded_document_id);


--
-- TOC entry 3281 (class 2606 OID 74680)
-- Name: insurance_contract_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY insurance_contract
    ADD CONSTRAINT insurance_contract_pkey PRIMARY KEY (id);


--
-- TOC entry 3285 (class 2606 OID 74690)
-- Name: insurance_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY insurance_ref
    ADD CONSTRAINT insurance_pk PRIMARY KEY (id);


--
-- TOC entry 3211 (class 2606 OID 74523)
-- Name: invoice_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY commercial_invoice
    ADD CONSTRAINT invoice_pk PRIMARY KEY (id);


--
-- TOC entry 3186 (class 2606 OID 74444)
-- Name: lang_id; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY lang
    ADD CONSTRAINT lang_id PRIMARY KEY (id);


--
-- TOC entry 3395 (class 2606 OID 78854)
-- Name: license_additional_document_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY license_additional_document
    ADD CONSTRAINT license_additional_document_pk PRIMARY KEY (id);


--
-- TOC entry 3387 (class 2606 OID 78794)
-- Name: license_document_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY license_document
    ADD CONSTRAINT license_document_pk PRIMARY KEY (id);


--
-- TOC entry 3389 (class 2606 OID 78799)
-- Name: license_invoice_document_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY license_invoice_document
    ADD CONSTRAINT license_invoice_document_pk PRIMARY KEY (id);


--
-- TOC entry 3393 (class 2606 OID 78834)
-- Name: license_product_document_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY license_product_document
    ADD CONSTRAINT license_product_document_pk PRIMARY KEY (id);


--
-- TOC entry 3391 (class 2606 OID 78814)
-- Name: license_request_document_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY license_request_document
    ADD CONSTRAINT license_request_document_pk PRIMARY KEY (id);


--
-- TOC entry 3385 (class 2606 OID 78756)
-- Name: license_request_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY license_request
    ADD CONSTRAINT license_request_pk PRIMARY KEY (id);


--
-- TOC entry 3383 (class 2606 OID 78736)
-- Name: license_shipment_info_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY license_shipment_info
    ADD CONSTRAINT license_shipment_info_pkey PRIMARY KEY (id);


--
-- TOC entry 3193 (class 2606 OID 74456)
-- Name: mandatory_document_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY attachement_document_ref
    ADD CONSTRAINT mandatory_document_pk PRIMARY KEY (id);


--
-- TOC entry 3287 (class 2606 OID 74698)
-- Name: member_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY member
    ADD CONSTRAINT member_pkey PRIMARY KEY (id);


--
-- TOC entry 3369 (class 2606 OID 77463)
-- Name: message_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- TOC entry 3291 (class 2606 OID 74705)
-- Name: ministry_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY ministry_ref
    ADD CONSTRAINT ministry_pk PRIMARY KEY (id);


--
-- TOC entry 3296 (class 2606 OID 74715)
-- Name: national_procedure_code_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY national_procedure_code_lang
    ADD CONSTRAINT national_procedure_code_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3298 (class 2606 OID 74724)
-- Name: operation_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY operation
    ADD CONSTRAINT operation_pkey PRIMARY KEY (id);


--
-- TOC entry 3300 (class 2606 OID 74732)
-- Name: organization_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT organization_pk PRIMARY KEY (id);


--
-- TOC entry 3375 (class 2606 OID 77813)
-- Name: packaging_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY packaging_lang
    ADD CONSTRAINT packaging_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3371 (class 2606 OID 77806)
-- Name: packaging_ref_id; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY packaging_ref
    ADD CONSTRAINT packaging_ref_id PRIMARY KEY (id);


--
-- TOC entry 3302 (class 2606 OID 74737)
-- Name: parc_vente_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY parc_vente_ref
    ADD CONSTRAINT parc_vente_pkey PRIMARY KEY (id);


--
-- TOC entry 3304 (class 2606 OID 74742)
-- Name: payment_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (id);


--
-- TOC entry 3306 (class 2606 OID 74747)
-- Name: permission_right_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY permission_right
    ADD CONSTRAINT permission_right_pkey PRIMARY KEY (id);


--
-- TOC entry 3312 (class 2606 OID 74757)
-- Name: port_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY port_lang
    ADD CONSTRAINT port_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3309 (class 2606 OID 74752)
-- Name: port_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY port_ref
    ADD CONSTRAINT port_pk PRIMARY KEY (id);


--
-- TOC entry 3271 (class 2606 OID 74658)
-- Name: procedure_code_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY extended_procedure_code_ref
    ADD CONSTRAINT procedure_code_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3397 (class 2606 OID 78869)
-- Name: product_additional_infos_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY product_additional_infos
    ADD CONSTRAINT product_additional_infos_pk PRIMARY KEY (id);


--
-- TOC entry 3317 (class 2606 OID 74767)
-- Name: product_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pk PRIMARY KEY (id);


--
-- TOC entry 3328 (class 2606 OID 74795)
-- Name: request_transport_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_pkey PRIMARY KEY (id);


--
-- TOC entry 3330 (class 2606 OID 74805)
-- Name: resource_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY resource
    ADD CONSTRAINT resource_pkey PRIMARY KEY (id);


--
-- TOC entry 3332 (class 2606 OID 74813)
-- Name: risk_list_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY risk_list
    ADD CONSTRAINT risk_list_pkey PRIMARY KEY (id);


--
-- TOC entry 3336 (class 2606 OID 74823)
-- Name: role_permission_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY role_permission
    ADD CONSTRAINT role_permission_pkey PRIMARY KEY (role_id, permission_id);


--
-- TOC entry 3334 (class 2606 OID 74818)
-- Name: role_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 3338 (class 2606 OID 74828)
-- Name: single_window_operator_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY single_window_operator_ref
    ADD CONSTRAINT single_window_operator_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3340 (class 2606 OID 74836)
-- Name: tariff_book_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY tariff_book_ref
    ADD CONSTRAINT tariff_book_pk PRIMARY KEY (id);


--
-- TOC entry 3342 (class 2606 OID 74841)
-- Name: transportation_type_ref_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY transportation_type_ref
    ADD CONSTRAINT transportation_type_ref_pk PRIMARY KEY (id);


--
-- TOC entry 3202 (class 2606 OID 74481)
-- Name: uk_authorization_customs_regim; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY authorization_ref_customs_regim
    ADD CONSTRAINT uk_authorization_customs_regim UNIQUE (authorization_ref_id, customs_regim_id);


--
-- TOC entry 3205 (class 2606 OID 74504)
-- Name: uk_authorization_tariff_book; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY authorization_ref_tariff_book
    ADD CONSTRAINT uk_authorization_tariff_book UNIQUE (authorization_id, tariff_book_id);


--
-- TOC entry 3226 (class 2606 OID 74550)
-- Name: uk_currency_code; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY currency_ref
    ADD CONSTRAINT uk_currency_code UNIQUE (code);


--
-- TOC entry 3242 (class 2606 OID 74587)
-- Name: uk_document_setup_customs_regim; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_customs_regim
    ADD CONSTRAINT uk_document_setup_customs_regim UNIQUE (document_setup_id, customs_regim_id);


--
-- TOC entry 3244 (class 2606 OID 74592)
-- Name: uk_document_setup_destination; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_destination
    ADD CONSTRAINT uk_document_setup_destination UNIQUE (document_setup_id, country_id);


--
-- TOC entry 3246 (class 2606 OID 74597)
-- Name: uk_document_setup_destination_exception; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_destination_exception
    ADD CONSTRAINT uk_document_setup_destination_exception UNIQUE (document_setup_id, country_id);


--
-- TOC entry 3248 (class 2606 OID 74602)
-- Name: uk_document_setup_extended_code; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_extended_code
    ADD CONSTRAINT uk_document_setup_extended_code UNIQUE (document_setup_id, extended_procedure_code_id);


--
-- TOC entry 3250 (class 2606 OID 74607)
-- Name: uk_document_setup_national_code; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_national_code
    ADD CONSTRAINT uk_document_setup_national_code UNIQUE (document_setup_id, national_procedure_code_id);


--
-- TOC entry 3252 (class 2606 OID 74612)
-- Name: uk_document_setup_origin; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_origin
    ADD CONSTRAINT uk_document_setup_origin UNIQUE (document_setup_id, country_id);


--
-- TOC entry 3254 (class 2606 OID 74617)
-- Name: uk_document_setup_origin_exception; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_origin_exception
    ADD CONSTRAINT uk_document_setup_origin_exception UNIQUE (document_setup_id, country_id);


--
-- TOC entry 3257 (class 2606 OID 74622)
-- Name: uk_document_setup_tariff_book; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_tariff_book
    ADD CONSTRAINT uk_document_setup_tariff_book UNIQUE (document_setup_id, tariff_book_id);


--
-- TOC entry 3259 (class 2606 OID 74628)
-- Name: uk_document_setup_visibility; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY document_setup_visibility
    ADD CONSTRAINT uk_document_setup_visibility UNIQUE (document_setup_id, organization_id);


--
-- TOC entry 3188 (class 2606 OID 74446)
-- Name: uk_lang_code; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY lang
    ADD CONSTRAINT uk_lang_code UNIQUE (code);


--
-- TOC entry 3373 (class 2606 OID 77808)
-- Name: uk_packaging_code; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY packaging_ref
    ADD CONSTRAINT uk_packaging_code UNIQUE (code);


--
-- TOC entry 3289 (class 2606 OID 74700)
-- Name: uk_username; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY member
    ADD CONSTRAINT uk_username UNIQUE (username);


--
-- TOC entry 3344 (class 2606 OID 74849)
-- Name: uploaded_document_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY uploaded_document
    ADD CONSTRAINT uploaded_document_pkey PRIMARY KEY (id);


--
-- TOC entry 3346 (class 2606 OID 74854)
-- Name: user_role_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (member_id, role_id);


--
-- TOC entry 3350 (class 2606 OID 74870)
-- Name: vehicle_form_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY vehicle_form
    ADD CONSTRAINT vehicle_form_pkey PRIMARY KEY (id);


--
-- TOC entry 3352 (class 2606 OID 74875)
-- Name: vehicle_information_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY vehicle_information
    ADD CONSTRAINT vehicle_information_pkey PRIMARY KEY (id);


--
-- TOC entry 3354 (class 2606 OID 74883)
-- Name: vehicle_operator_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY vehicle_operator
    ADD CONSTRAINT vehicle_operator_pkey PRIMARY KEY (id);


--
-- TOC entry 3348 (class 2606 OID 74862)
-- Name: vehicle_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_pkey PRIMARY KEY (id);


--
-- TOC entry 3381 (class 2606 OID 78657)
-- Name: vehicle_product_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY vehicle_product
    ADD CONSTRAINT vehicle_product_pk PRIMARY KEY (id);


--
-- TOC entry 3359 (class 2606 OID 74896)
-- Name: warehouse_lang_pk; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY warehouse_lang
    ADD CONSTRAINT warehouse_lang_pk PRIMARY KEY (id);


--
-- TOC entry 3356 (class 2606 OID 74891)
-- Name: warehouse_ref_pkey; Type: CONSTRAINT; Schema: epermits; Owner: epermits; Tablespace: 
--

ALTER TABLE ONLY warehouse_ref
    ADD CONSTRAINT warehouse_ref_pkey PRIMARY KEY (id);


--
-- TOC entry 3191 (class 1259 OID 74457)
-- Name: IX_attachement_document_tariff_book; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_attachement_document_tariff_book" ON attachement_document_ref USING btree (tariff_book_id);


--
-- TOC entry 3203 (class 1259 OID 74505)
-- Name: IX_authorization_tariff_book_tariff_book_ref; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_authorization_tariff_book_tariff_book_ref" ON authorization_ref_tariff_book USING btree (tariff_book_id);


--
-- TOC entry 3255 (class 1259 OID 74623)
-- Name: IX_document_setup_tariff_book_tariff_book_ref; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_document_setup_tariff_book_tariff_book_ref" ON document_setup_tariff_book USING btree (tariff_book_id);


--
-- TOC entry 3198 (class 1259 OID 74476)
-- Name: IX_mandatory_epermits_organisation; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_mandatory_epermits_organisation" ON authorization_ref USING btree (organisation_id);


--
-- TOC entry 3313 (class 1259 OID 74769)
-- Name: IX_product_country; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_product_country" ON product USING btree (country_id);


--
-- TOC entry 3314 (class 1259 OID 74770)
-- Name: IX_product_invoice; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_product_invoice" ON product USING btree (invoice_id);


--
-- TOC entry 3315 (class 1259 OID 74771)
-- Name: IX_product_tarrif_book; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_product_tarrif_book" ON product USING btree (tariff_book_id);


--
-- TOC entry 3320 (class 1259 OID 74785)
-- Name: IX_request_clearing_agency; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_request_clearing_agency" ON request USING btree (clearing_agency_id);


--
-- TOC entry 3321 (class 1259 OID 74786)
-- Name: IX_request_consignee; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_request_consignee" ON request USING btree (consignee_id);


--
-- TOC entry 3322 (class 1259 OID 74787)
-- Name: IX_request_consignor; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX "IX_request_consignor" ON request USING btree (consignor_id);


--
-- TOC entry 3307 (class 1259 OID 74758)
-- Name: ix_code_port; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_code_port ON port_ref USING btree (code);


--
-- TOC entry 3212 (class 1259 OID 74524)
-- Name: ix_commercial_invoice_currency; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_commercial_invoice_currency ON commercial_invoice USING btree (currency_id);


--
-- TOC entry 3213 (class 1259 OID 74525)
-- Name: ix_commercial_invoice_epermits_request; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_commercial_invoice_epermits_request ON commercial_invoice USING btree (request_id);


--
-- TOC entry 3214 (class 1259 OID 74526)
-- Name: ix_commercial_invoice_uploaded_document; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_commercial_invoice_uploaded_document ON commercial_invoice USING btree (uploaded_document_id);


--
-- TOC entry 3217 (class 1259 OID 74532)
-- Name: ix_container_request; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_container_request ON container USING btree (request_id);


--
-- TOC entry 3222 (class 1259 OID 74543)
-- Name: ix_label_country; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_country ON country_lang USING btree (label);


--
-- TOC entry 3229 (class 1259 OID 74556)
-- Name: ix_label_currency; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_currency ON currency_lang USING btree (label);


--
-- TOC entry 3234 (class 1259 OID 74567)
-- Name: ix_label_customs_office; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_customs_office ON customs_office_lang USING btree (label);


--
-- TOC entry 3274 (class 1259 OID 74664)
-- Name: ix_label_extended_procedure_code; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_extended_procedure_code ON extended_procedure_code_lang USING btree (label);


--
-- TOC entry 3279 (class 1259 OID 74675)
-- Name: ix_label_goods_location; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_goods_location ON goods_location_lang USING btree (label);


--
-- TOC entry 3294 (class 1259 OID 74716)
-- Name: ix_label_national_procedure_code; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_national_procedure_code ON national_procedure_code_lang USING btree (label);


--
-- TOC entry 3310 (class 1259 OID 74759)
-- Name: ix_label_port; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_port ON port_lang USING btree (label);


--
-- TOC entry 3357 (class 1259 OID 74897)
-- Name: ix_label_warehouse; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_label_warehouse ON warehouse_lang USING btree (label);


--
-- TOC entry 3325 (class 1259 OID 74796)
-- Name: ix_request_transport_country_transit; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_request_transport_country_transit ON request_transport USING btree (country_transit_id);


--
-- TOC entry 3326 (class 1259 OID 74797)
-- Name: ix_request_transport_uploaded_documents; Type: INDEX; Schema: epermits; Owner: epermits; Tablespace: 
--

CREATE INDEX ix_request_transport_uploaded_documents ON request_transport USING btree (uploaded_document_id);


--
-- TOC entry 3399 (class 2606 OID 74903)
-- Name: attach_doc_org_join_attachement_document_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY attach_doc_org_join
    ADD CONSTRAINT attach_doc_org_join_attachement_document_ref_fk FOREIGN KEY (attachement_document_id) REFERENCES attachement_document_ref(id);


--
-- TOC entry 3398 (class 2606 OID 74898)
-- Name: attach_doc_org_join_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY attach_doc_org_join
    ADD CONSTRAINT attach_doc_org_join_organization_fk FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- TOC entry 3400 (class 2606 OID 74908)
-- Name: attachement_document_tariff_book; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY attachement_document_ref
    ADD CONSTRAINT attachement_document_tariff_book FOREIGN KEY (tariff_book_id) REFERENCES tariff_book_ref(id);


--
-- TOC entry 3521 (class 2606 OID 75413)
-- Name: author_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY risk_list
    ADD CONSTRAINT author_organization_fk FOREIGN KEY (author_id) REFERENCES organization(id);


--
-- TOC entry 3404 (class 2606 OID 74923)
-- Name: authorization_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY "authorization"
    ADD CONSTRAINT authorization_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3409 (class 2606 OID 74948)
-- Name: authorization_customs_regim_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_customs_regim
    ADD CONSTRAINT authorization_customs_regim_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3408 (class 2606 OID 74943)
-- Name: authorization_customs_regim_customs_regim_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_customs_regim
    ADD CONSTRAINT authorization_customs_regim_customs_regim_ref_fk FOREIGN KEY (customs_regim_id) REFERENCES customs_regim_ref(id);


--
-- TOC entry 3401 (class 2606 OID 78875)
-- Name: authorization_license_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY "authorization"
    ADD CONSTRAINT authorization_license_request_fk FOREIGN KEY (license_request_id) REFERENCES license_request(id);


--
-- TOC entry 3405 (class 2606 OID 74928)
-- Name: authorization_product_join_authorization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_product_join
    ADD CONSTRAINT authorization_product_join_authorization_fk FOREIGN KEY (authorization_id) REFERENCES "authorization"(id);


--
-- TOC entry 3406 (class 2606 OID 74933)
-- Name: authorization_product_join_product_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_product_join
    ADD CONSTRAINT authorization_product_join_product_fk FOREIGN KEY (product_id) REFERENCES product(id);


--
-- TOC entry 3550 (class 2606 OID 77342)
-- Name: authorization_ref_ancestor_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_predecessor
    ADD CONSTRAINT authorization_ref_ancestor_fk FOREIGN KEY (ancestor_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3407 (class 2606 OID 74938)
-- Name: authorization_ref_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref
    ADD CONSTRAINT authorization_ref_organization_fk FOREIGN KEY (organisation_id) REFERENCES organization(id);


--
-- TOC entry 3549 (class 2606 OID 77337)
-- Name: authorization_ref_predecessor_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_predecessor
    ADD CONSTRAINT authorization_ref_predecessor_fk FOREIGN KEY (predecessor_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3423 (class 2606 OID 75018)
-- Name: authorization_tariff_book_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_tariff_book
    ADD CONSTRAINT authorization_tariff_book_authorization_ref_fk FOREIGN KEY (authorization_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3422 (class 2606 OID 75013)
-- Name: authorization_tariff_book_tariff_book_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_tariff_book
    ADD CONSTRAINT authorization_tariff_book_tariff_book_ref_fk FOREIGN KEY (tariff_book_id) REFERENCES tariff_book_ref(id);


--
-- TOC entry 3403 (class 2606 OID 74918)
-- Name: authorization_vehicle_form_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY "authorization"
    ADD CONSTRAINT authorization_vehicle_form_fk FOREIGN KEY (vehicle_form_id) REFERENCES vehicle_form(id);


--
-- TOC entry 3424 (class 2606 OID 75023)
-- Name: bank_account_bank_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY bank_account
    ADD CONSTRAINT bank_account_bank_fk FOREIGN KEY (bank_id) REFERENCES bank_ref(id);


--
-- TOC entry 3425 (class 2606 OID 75028)
-- Name: bank_account_economic_operator_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY bank_account
    ADD CONSTRAINT bank_account_economic_operator_fk FOREIGN KEY (economic_operator_id) REFERENCES economic_operator(id);


--
-- TOC entry 3426 (class 2606 OID 75033)
-- Name: bank_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY bank_ref
    ADD CONSTRAINT bank_organization_fk FOREIGN KEY (id) REFERENCES organization(id);


--
-- TOC entry 3430 (class 2606 OID 75048)
-- Name: commercial_invoice_curreny_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY commercial_invoice
    ADD CONSTRAINT commercial_invoice_curreny_fk FOREIGN KEY (currency_id) REFERENCES currency_ref(id);


--
-- TOC entry 3427 (class 2606 OID 78782)
-- Name: commercial_invoice_license_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY commercial_invoice
    ADD CONSTRAINT commercial_invoice_license_request_fk FOREIGN KEY (license_request_id) REFERENCES license_request(id);


--
-- TOC entry 3428 (class 2606 OID 75038)
-- Name: commercial_invoice_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY commercial_invoice
    ADD CONSTRAINT commercial_invoice_request_fk FOREIGN KEY (request_id) REFERENCES request(id);


--
-- TOC entry 3429 (class 2606 OID 75043)
-- Name: commercial_invoice_uploaded_documents_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY commercial_invoice
    ADD CONSTRAINT commercial_invoice_uploaded_documents_fk FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document(id);


--
-- TOC entry 3555 (class 2606 OID 78065)
-- Name: container_product_join_container_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY container_product_join
    ADD CONSTRAINT container_product_join_container_fk FOREIGN KEY (container_id) REFERENCES container(id);


--
-- TOC entry 3556 (class 2606 OID 78070)
-- Name: container_product_join_product_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY container_product_join
    ADD CONSTRAINT container_product_join_product_fk FOREIGN KEY (product_id) REFERENCES product(id);


--
-- TOC entry 3431 (class 2606 OID 75053)
-- Name: container_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY container
    ADD CONSTRAINT container_request_fk FOREIGN KEY (request_id) REFERENCES request(id);


--
-- TOC entry 3433 (class 2606 OID 75533)
-- Name: country_lang_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY country_lang
    ADD CONSTRAINT country_lang_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3432 (class 2606 OID 75528)
-- Name: country_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY country_lang
    ADD CONSTRAINT country_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3480 (class 2606 OID 75233)
-- Name: country_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT country_organization_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3435 (class 2606 OID 75523)
-- Name: currency_lang_currency_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY currency_lang
    ADD CONSTRAINT currency_lang_currency_fk FOREIGN KEY (currency_id) REFERENCES currency_ref(id);


--
-- TOC entry 3434 (class 2606 OID 75518)
-- Name: currency_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY currency_lang
    ADD CONSTRAINT currency_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3437 (class 2606 OID 75593)
-- Name: customs_office_lang_currency_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY customs_office_lang
    ADD CONSTRAINT customs_office_lang_currency_fk FOREIGN KEY (customs_office_id) REFERENCES customs_office_ref(id);


--
-- TOC entry 3436 (class 2606 OID 75588)
-- Name: customs_office_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY customs_office_lang
    ADD CONSTRAINT customs_office_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3438 (class 2606 OID 75058)
-- Name: customs_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY customs_ref
    ADD CONSTRAINT customs_organization_fk FOREIGN KEY (id) REFERENCES organization(id);


--
-- TOC entry 3548 (class 2606 OID 76368)
-- Name: customs_regim_lang_currency_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY customs_regim_lang
    ADD CONSTRAINT customs_regim_lang_currency_fk FOREIGN KEY (customs_regim_id) REFERENCES customs_regim_ref(id);


--
-- TOC entry 3547 (class 2606 OID 76363)
-- Name: customs_regim_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY customs_regim_lang
    ADD CONSTRAINT customs_regim_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3411 (class 2606 OID 74958)
-- Name: destination_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_destination
    ADD CONSTRAINT destination_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3410 (class 2606 OID 74953)
-- Name: destination_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_destination
    ADD CONSTRAINT destination_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3412 (class 2606 OID 74963)
-- Name: destination_exception_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_destination_exception
    ADD CONSTRAINT destination_exception_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3413 (class 2606 OID 74968)
-- Name: destination_exception_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_destination_exception
    ADD CONSTRAINT destination_exception_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3440 (class 2606 OID 75068)
-- Name: document_setup_customs_regim_customs_regim_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_customs_regim
    ADD CONSTRAINT document_setup_customs_regim_customs_regim_ref_fk FOREIGN KEY (customs_regim_id) REFERENCES customs_regim_ref(id);


--
-- TOC entry 3439 (class 2606 OID 75063)
-- Name: document_setup_customs_regim_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_customs_regim
    ADD CONSTRAINT document_setup_customs_regim_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3441 (class 2606 OID 75073)
-- Name: document_setup_destination_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_destination
    ADD CONSTRAINT document_setup_destination_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3442 (class 2606 OID 75078)
-- Name: document_setup_destination_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_destination
    ADD CONSTRAINT document_setup_destination_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3444 (class 2606 OID 75088)
-- Name: document_setup_destination_exception_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_destination_exception
    ADD CONSTRAINT document_setup_destination_exception_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3443 (class 2606 OID 75083)
-- Name: document_setup_destination_exception_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_destination_exception
    ADD CONSTRAINT document_setup_destination_exception_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3446 (class 2606 OID 75098)
-- Name: document_setup_extended_code_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_extended_code
    ADD CONSTRAINT document_setup_extended_code_authorization_ref_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3445 (class 2606 OID 75093)
-- Name: document_setup_extended_code_extended_procedure_code_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_extended_code
    ADD CONSTRAINT document_setup_extended_code_extended_procedure_code_fk FOREIGN KEY (extended_procedure_code_id) REFERENCES extended_procedure_code_ref(id);


--
-- TOC entry 3448 (class 2606 OID 75108)
-- Name: document_setup_national_code_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_national_code
    ADD CONSTRAINT document_setup_national_code_authorization_ref_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3447 (class 2606 OID 75103)
-- Name: document_setup_national_code_national_procedure_code_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_national_code
    ADD CONSTRAINT document_setup_national_code_national_procedure_code_fk FOREIGN KEY (national_procedure_code_id) REFERENCES national_procedure_code_ref(id);


--
-- TOC entry 3450 (class 2606 OID 75118)
-- Name: document_setup_origin_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_origin
    ADD CONSTRAINT document_setup_origin_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3449 (class 2606 OID 75113)
-- Name: document_setup_origin_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_origin
    ADD CONSTRAINT document_setup_origin_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3452 (class 2606 OID 75128)
-- Name: document_setup_origin_exception_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_origin_exception
    ADD CONSTRAINT document_setup_origin_exception_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3451 (class 2606 OID 75123)
-- Name: document_setup_origin_exception_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_origin_exception
    ADD CONSTRAINT document_setup_origin_exception_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3454 (class 2606 OID 75138)
-- Name: document_setup_tariff_book_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_tariff_book
    ADD CONSTRAINT document_setup_tariff_book_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3453 (class 2606 OID 75133)
-- Name: document_setup_tariff_book_tariff_book_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_tariff_book
    ADD CONSTRAINT document_setup_tariff_book_tariff_book_ref_fk FOREIGN KEY (tariff_book_id) REFERENCES tariff_book_ref(id);


--
-- TOC entry 3455 (class 2606 OID 75143)
-- Name: document_setup_visibility_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_visibility
    ADD CONSTRAINT document_setup_visibility_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3456 (class 2606 OID 75148)
-- Name: document_setup_visibility_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY document_setup_visibility
    ADD CONSTRAINT document_setup_visibility_organization_fk FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- TOC entry 3458 (class 2606 OID 75158)
-- Name: economic_operator_document_join_economic_operator_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY economic_operator_document_join
    ADD CONSTRAINT economic_operator_document_join_economic_operator_fk FOREIGN KEY (economic_operator_id) REFERENCES economic_operator(id);


--
-- TOC entry 3459 (class 2606 OID 75163)
-- Name: economic_operator_document_join_uploaded_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY economic_operator_document_join
    ADD CONSTRAINT economic_operator_document_join_uploaded_document_fk FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document(id);


--
-- TOC entry 3457 (class 2606 OID 75153)
-- Name: economic_operator_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY economic_operator
    ADD CONSTRAINT economic_operator_organization_fk FOREIGN KEY (id) REFERENCES organization(id);


--
-- TOC entry 3414 (class 2606 OID 74973)
-- Name: epermit_extended_code_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_extended_code
    ADD CONSTRAINT epermit_extended_code_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3415 (class 2606 OID 74978)
-- Name: epermit_extended_code_extended_procedure_code_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_extended_code
    ADD CONSTRAINT epermit_extended_code_extended_procedure_code_fk FOREIGN KEY (extended_procedure_code_id) REFERENCES extended_procedure_code_ref(id);


--
-- TOC entry 3460 (class 2606 OID 75168)
-- Name: epermit_invoice_line_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY epermit_invoice_line
    ADD CONSTRAINT epermit_invoice_line_organization_fk FOREIGN KEY (ogranization_id) REFERENCES organization(id);


--
-- TOC entry 3461 (class 2606 OID 75173)
-- Name: epermit_invoice_line_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY epermit_invoice_line
    ADD CONSTRAINT epermit_invoice_line_request_fk FOREIGN KEY (request_id) REFERENCES request(id);


--
-- TOC entry 3416 (class 2606 OID 74983)
-- Name: epermit_national_code_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_national_code
    ADD CONSTRAINT epermit_national_code_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3417 (class 2606 OID 74988)
-- Name: epermit_national_code_national_procedure_code_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_national_code
    ADD CONSTRAINT epermit_national_code_national_procedure_code_fk FOREIGN KEY (national_procedure_code_id) REFERENCES national_procedure_code_ref(id);


--
-- TOC entry 3463 (class 2606 OID 75183)
-- Name: exchange_rate_currency_from_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY exchange_rate
    ADD CONSTRAINT exchange_rate_currency_from_fk FOREIGN KEY (currency_from_id) REFERENCES currency_ref(id);


--
-- TOC entry 3462 (class 2606 OID 75178)
-- Name: exchange_rate_currency_to_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY exchange_rate
    ADD CONSTRAINT exchange_rate_currency_to_fk FOREIGN KEY (currency_to_id) REFERENCES currency_ref(id);


--
-- TOC entry 3464 (class 2606 OID 75188)
-- Name: extended_code_custom_regime_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY extended_procedure_code_ref
    ADD CONSTRAINT extended_code_custom_regime_fk FOREIGN KEY (custom_regime_id) REFERENCES customs_regim_ref(id);


--
-- TOC entry 3466 (class 2606 OID 75543)
-- Name: extended_procedure_code_lang_epc_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY extended_procedure_code_lang
    ADD CONSTRAINT extended_procedure_code_lang_epc_fk FOREIGN KEY (extended_procedure_code_id) REFERENCES extended_procedure_code_ref(id);


--
-- TOC entry 3465 (class 2606 OID 75538)
-- Name: extended_procedure_code_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY extended_procedure_code_lang
    ADD CONSTRAINT extended_procedure_code_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3402 (class 2606 OID 74913)
-- Name: generated_document_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY "authorization"
    ADD CONSTRAINT generated_document_request_fk FOREIGN KEY (request_id) REFERENCES request(id);


--
-- TOC entry 3468 (class 2606 OID 75553)
-- Name: goods_location_lang_goods_location_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY goods_location_lang
    ADD CONSTRAINT goods_location_lang_goods_location_fk FOREIGN KEY (goods_location_ref_id) REFERENCES goods_location_ref(id);


--
-- TOC entry 3467 (class 2606 OID 75548)
-- Name: goods_location_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY goods_location_lang
    ADD CONSTRAINT goods_location_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3546 (class 2606 OID 76353)
-- Name: http_message_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY http_message_lang
    ADD CONSTRAINT http_message_fk FOREIGN KEY (http_message_id) REFERENCES http_message(id);


--
-- TOC entry 3545 (class 2606 OID 76348)
-- Name: http_message_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY http_message_lang
    ADD CONSTRAINT http_message_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3471 (class 2606 OID 75203)
-- Name: insurance_contract_document_join_insurance_contract_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY insurance_contract_document_join
    ADD CONSTRAINT insurance_contract_document_join_insurance_contract_fk FOREIGN KEY (insurance_contract_id) REFERENCES insurance_contract(id);


--
-- TOC entry 3472 (class 2606 OID 75208)
-- Name: insurance_contract_document_join_uploaded_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY insurance_contract_document_join
    ADD CONSTRAINT insurance_contract_document_join_uploaded_document_fk FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document(id);


--
-- TOC entry 3470 (class 2606 OID 75198)
-- Name: insurance_contract_economic_operator_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY insurance_contract
    ADD CONSTRAINT insurance_contract_economic_operator_fk FOREIGN KEY (economic_operator_id) REFERENCES economic_operator(id);


--
-- TOC entry 3469 (class 2606 OID 75193)
-- Name: insurance_contract_insurance_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY insurance_contract
    ADD CONSTRAINT insurance_contract_insurance_fk FOREIGN KEY (insurance_id) REFERENCES insurance_ref(id);


--
-- TOC entry 3473 (class 2606 OID 75213)
-- Name: insurance_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY insurance_ref
    ADD CONSTRAINT insurance_organization_fk FOREIGN KEY (id) REFERENCES organization(id);


--
-- TOC entry 3573 (class 2606 OID 78855)
-- Name: license_additional_document_license_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_additional_document
    ADD CONSTRAINT license_additional_document_license_document_fk FOREIGN KEY (id) REFERENCES license_document(id);


--
-- TOC entry 3574 (class 2606 OID 78860)
-- Name: license_additional_document_license_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_additional_document
    ADD CONSTRAINT license_additional_document_license_request_fk FOREIGN KEY (license_request_id) REFERENCES license_request(id);


--
-- TOC entry 3561 (class 2606 OID 78762)
-- Name: license_consignee_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request
    ADD CONSTRAINT license_consignee_fk FOREIGN KEY (consignee_id) REFERENCES organization(id);


--
-- TOC entry 3562 (class 2606 OID 78767)
-- Name: license_consignor_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request
    ADD CONSTRAINT license_consignor_fk FOREIGN KEY (consignor_id) REFERENCES organization(id);


--
-- TOC entry 3563 (class 2606 OID 78772)
-- Name: license_economic_operator_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request
    ADD CONSTRAINT license_economic_operator_fk FOREIGN KEY (clearing_agency_id) REFERENCES economic_operator(id);


--
-- TOC entry 3566 (class 2606 OID 78805)
-- Name: license_invoice_document_commercial_invoice_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_invoice_document
    ADD CONSTRAINT license_invoice_document_commercial_invoice_fk FOREIGN KEY (commercial_invoice_id) REFERENCES commercial_invoice(id);


--
-- TOC entry 3565 (class 2606 OID 78800)
-- Name: license_invoice_document_license_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_invoice_document
    ADD CONSTRAINT license_invoice_document_license_document_fk FOREIGN KEY (id) REFERENCES license_document(id);


--
-- TOC entry 3564 (class 2606 OID 78777)
-- Name: license_member_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request
    ADD CONSTRAINT license_member_fk FOREIGN KEY (member_id) REFERENCES member(id);


--
-- TOC entry 3571 (class 2606 OID 78840)
-- Name: license_product_document_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_product_document
    ADD CONSTRAINT license_product_document_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3570 (class 2606 OID 78835)
-- Name: license_product_document_license_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_product_document
    ADD CONSTRAINT license_product_document_license_document_fk FOREIGN KEY (id) REFERENCES license_document(id);


--
-- TOC entry 3572 (class 2606 OID 78845)
-- Name: license_product_document_product_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_product_document
    ADD CONSTRAINT license_product_document_product_fk FOREIGN KEY (product_id) REFERENCES product(id);


--
-- TOC entry 3569 (class 2606 OID 78825)
-- Name: license_request_document_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request_document
    ADD CONSTRAINT license_request_document_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3567 (class 2606 OID 78815)
-- Name: license_request_document_license_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request_document
    ADD CONSTRAINT license_request_document_license_document_fk FOREIGN KEY (id) REFERENCES license_document(id);


--
-- TOC entry 3568 (class 2606 OID 78820)
-- Name: license_request_document_license_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request_document
    ADD CONSTRAINT license_request_document_license_request_fk FOREIGN KEY (license_request_id) REFERENCES license_request(id);


--
-- TOC entry 3560 (class 2606 OID 78757)
-- Name: license_request_license_shipment_info_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_request
    ADD CONSTRAINT license_request_license_shipment_info_fk FOREIGN KEY (shipment_info_id) REFERENCES license_shipment_info(id);


--
-- TOC entry 3559 (class 2606 OID 78747)
-- Name: license_shipment_info_country_destination_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_shipment_info
    ADD CONSTRAINT license_shipment_info_country_destination_fk FOREIGN KEY (country_destination_id) REFERENCES country_ref(id);


--
-- TOC entry 3558 (class 2606 OID 78742)
-- Name: license_shipment_info_country_export_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_shipment_info
    ADD CONSTRAINT license_shipment_info_country_export_fk FOREIGN KEY (country_export_id) REFERENCES country_ref(id);


--
-- TOC entry 3557 (class 2606 OID 78737)
-- Name: license_shipment_info_country_supply_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY license_shipment_info
    ADD CONSTRAINT license_shipment_info_country_supply_fk FOREIGN KEY (country_supply_id) REFERENCES country_ref(id);


--
-- TOC entry 3474 (class 2606 OID 75218)
-- Name: member_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY member
    ADD CONSTRAINT member_organization_fk FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- TOC entry 3551 (class 2606 OID 77464)
-- Name: message_authorization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_authorization_fk FOREIGN KEY (authorization_id) REFERENCES "authorization"(id);


--
-- TOC entry 3475 (class 2606 OID 75223)
-- Name: ministry_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY ministry_ref
    ADD CONSTRAINT ministry_organization_fk FOREIGN KEY (id) REFERENCES organization(id);


--
-- TOC entry 3476 (class 2606 OID 75228)
-- Name: national_code_extended_code_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY national_procedure_code_ref
    ADD CONSTRAINT national_code_extended_code_fk FOREIGN KEY (extended_procedure_code_id) REFERENCES extended_procedure_code_ref(id);


--
-- TOC entry 3477 (class 2606 OID 75558)
-- Name: national_procedure_code_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY national_procedure_code_lang
    ADD CONSTRAINT national_procedure_code_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3478 (class 2606 OID 75563)
-- Name: national_procedure_code_npc_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY national_procedure_code_lang
    ADD CONSTRAINT national_procedure_code_npc_fk FOREIGN KEY (national_procedure_code_id) REFERENCES national_procedure_code_ref(id);


--
-- TOC entry 3479 (class 2606 OID 77327)
-- Name: nationality_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT nationality_organization_fk FOREIGN KEY (manager_nationality) REFERENCES country_ref(id);


--
-- TOC entry 3419 (class 2606 OID 74998)
-- Name: origin_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_origin
    ADD CONSTRAINT origin_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3418 (class 2606 OID 74993)
-- Name: origin_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_origin
    ADD CONSTRAINT origin_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3420 (class 2606 OID 75003)
-- Name: origin_exception_authorization_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_origin_exception
    ADD CONSTRAINT origin_exception_authorization_ref_fk FOREIGN KEY (authorization_ref_id) REFERENCES authorization_ref(id);


--
-- TOC entry 3421 (class 2606 OID 75008)
-- Name: origin_exception_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY authorization_ref_origin_exception
    ADD CONSTRAINT origin_exception_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3554 (class 2606 OID 77819)
-- Name: packaging_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY packaging_lang
    ADD CONSTRAINT packaging_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3553 (class 2606 OID 77814)
-- Name: packaging_lang_packaging_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY packaging_lang
    ADD CONSTRAINT packaging_lang_packaging_fk FOREIGN KEY (packaging_id) REFERENCES packaging_ref(id);


--
-- TOC entry 3481 (class 2606 OID 75238)
-- Name: parent_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT parent_organization_fk FOREIGN KEY (organization_parent_id) REFERENCES organization(id);


--
-- TOC entry 3483 (class 2606 OID 75248)
-- Name: payment_bank_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_bank_fk FOREIGN KEY (bank_id) REFERENCES bank_ref(id);


--
-- TOC entry 3482 (class 2606 OID 75243)
-- Name: payment_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_request_fk FOREIGN KEY (request_id) REFERENCES request(id);


--
-- TOC entry 3485 (class 2606 OID 75258)
-- Name: permission_right_operation_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY permission_right
    ADD CONSTRAINT permission_right_operation_fk FOREIGN KEY (operation_id) REFERENCES operation(id);


--
-- TOC entry 3484 (class 2606 OID 75253)
-- Name: permission_right_resource_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY permission_right
    ADD CONSTRAINT permission_right_resource_fk FOREIGN KEY (resource_id) REFERENCES resource(id);


--
-- TOC entry 3487 (class 2606 OID 75568)
-- Name: port_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY port_lang
    ADD CONSTRAINT port_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


--
-- TOC entry 3488 (class 2606 OID 75573)
-- Name: port_lang_port_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY port_lang
    ADD CONSTRAINT port_lang_port_fk FOREIGN KEY (port_id) REFERENCES port_ref(id);


--
-- TOC entry 3486 (class 2606 OID 75263)
-- Name: port_ref_country_ref_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY port_ref
    ADD CONSTRAINT port_ref_country_ref_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3491 (class 2606 OID 75268)
-- Name: product_additional_code_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_additional_code_fk FOREIGN KEY (national_procedure_code_id) REFERENCES national_procedure_code_ref(id);


--
-- TOC entry 3575 (class 2606 OID 78870)
-- Name: product_additional_infos_product_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product_additional_infos
    ADD CONSTRAINT product_additional_infos_product_fk FOREIGN KEY (product_id) REFERENCES product(id);


--
-- TOC entry 3494 (class 2606 OID 75288)
-- Name: product_commercial_invoice_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_commercial_invoice_fk FOREIGN KEY (invoice_id) REFERENCES commercial_invoice(id);


--
-- TOC entry 3493 (class 2606 OID 75278)
-- Name: product_country_destination_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_country_destination_fk FOREIGN KEY (country_destination_id) REFERENCES country_ref(id);


--
-- TOC entry 3492 (class 2606 OID 75273)
-- Name: product_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3498 (class 2606 OID 75308)
-- Name: product_document_join_product_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product_document_join
    ADD CONSTRAINT product_document_join_product_fk FOREIGN KEY (product_id) REFERENCES product(id);


--
-- TOC entry 3497 (class 2606 OID 75303)
-- Name: product_document_join_uploaded_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product_document_join
    ADD CONSTRAINT product_document_join_uploaded_document_fk FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document(id);


--
-- TOC entry 3490 (class 2606 OID 77824)
-- Name: product_packaging_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_packaging_fk FOREIGN KEY (packaging_id) REFERENCES packaging_ref(id);


--
-- TOC entry 3496 (class 2606 OID 75298)
-- Name: product_procedure_code_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_procedure_code_fk FOREIGN KEY (extended_procedure_code_id) REFERENCES extended_procedure_code_ref(id);


--
-- TOC entry 3495 (class 2606 OID 75293)
-- Name: product_tariff_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_tariff_fk FOREIGN KEY (tariff_book_id) REFERENCES tariff_book_ref(id);


--
-- TOC entry 3489 (class 2606 OID 78658)
-- Name: product_vehicle_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_vehicle_fk FOREIGN KEY (vehicle_id) REFERENCES vehicle_product(id);


--
-- TOC entry 3501 (class 2606 OID 75318)
-- Name: request_bank_account_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_bank_account_fk FOREIGN KEY (bank_account_change_id) REFERENCES bank_account(id);


--
-- TOC entry 3502 (class 2606 OID 75323)
-- Name: request_bank_account_payment_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_bank_account_payment_fk FOREIGN KEY (bank_account_payment_id) REFERENCES bank_account(id);


--
-- TOC entry 3499 (class 2606 OID 78176)
-- Name: request_bv_invoice_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_bv_invoice_fk FOREIGN KEY (bv_invoice_id) REFERENCES bv_invoice(id);


--
-- TOC entry 3503 (class 2606 OID 75328)
-- Name: request_consignee_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_consignee_fk FOREIGN KEY (consignee_id) REFERENCES organization(id);


--
-- TOC entry 3504 (class 2606 OID 75333)
-- Name: request_consignor_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_consignor_fk FOREIGN KEY (consignor_id) REFERENCES organization(id);


--
-- TOC entry 3505 (class 2606 OID 75338)
-- Name: request_custom_regime_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_custom_regime_fk FOREIGN KEY (custom_regime_id) REFERENCES customs_regim_ref(id);


--
-- TOC entry 3506 (class 2606 OID 75343)
-- Name: request_economic_operator_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_economic_operator_fk FOREIGN KEY (clearing_agency_id) REFERENCES economic_operator(id);


--
-- TOC entry 3507 (class 2606 OID 75348)
-- Name: request_goods_location_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_goods_location_fk FOREIGN KEY (goods_location_id) REFERENCES goods_location_ref(id);


--
-- TOC entry 3508 (class 2606 OID 75353)
-- Name: request_insurance_contract_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_insurance_contract_fk FOREIGN KEY (insurance_contract_id) REFERENCES insurance_contract(id);


--
-- TOC entry 3500 (class 2606 OID 75313)
-- Name: request_member_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_member_fk FOREIGN KEY (member_id) REFERENCES member(id);


--
-- TOC entry 3509 (class 2606 OID 75358)
-- Name: request_port_incoterm_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_port_incoterm_fk FOREIGN KEY (port_incoterm_id) REFERENCES port_ref(id);


--
-- TOC entry 3519 (class 2606 OID 75403)
-- Name: request_transport_country_transit_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_country_transit_fk FOREIGN KEY (country_transit_id) REFERENCES country_ref(id);


--
-- TOC entry 3518 (class 2606 OID 75398)
-- Name: request_transport_declaration_office_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_declaration_office_fk FOREIGN KEY (declaration_office_id) REFERENCES customs_office_ref(id);


--
-- TOC entry 3517 (class 2606 OID 75393)
-- Name: request_transport_entry_exit_office_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_entry_exit_office_fk FOREIGN KEY (entry_exit_office_id) REFERENCES customs_office_ref(id);


--
-- TOC entry 3516 (class 2606 OID 75388)
-- Name: request_transport_port_loading_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_port_loading_fk FOREIGN KEY (loading_port_id) REFERENCES port_ref(id);


--
-- TOC entry 3510 (class 2606 OID 77714)
-- Name: request_transport_port_loading_unloading_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_port_loading_unloading_fk FOREIGN KEY (loading_unloading_port_id) REFERENCES port_ref(id);


--
-- TOC entry 3515 (class 2606 OID 75383)
-- Name: request_transport_port_transit_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_port_transit_fk FOREIGN KEY (transit_port_id) REFERENCES port_ref(id);


--
-- TOC entry 3514 (class 2606 OID 75378)
-- Name: request_transport_port_unloading_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_port_unloading_fk FOREIGN KEY (unloading_port_id) REFERENCES port_ref(id);


--
-- TOC entry 3513 (class 2606 OID 75373)
-- Name: request_transport_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_request_fk FOREIGN KEY (request_id) REFERENCES request(id);


--
-- TOC entry 3512 (class 2606 OID 75368)
-- Name: request_transport_uploaded_document_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_uploaded_document_fk FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document(id);


--
-- TOC entry 3511 (class 2606 OID 75363)
-- Name: request_transport_uploaded_documents_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_uploaded_documents_fk FOREIGN KEY (uploaded_document_id) REFERENCES uploaded_document(id);


--
-- TOC entry 3520 (class 2606 OID 75408)
-- Name: request_transport_warehouse_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY request_transport
    ADD CONSTRAINT request_transport_warehouse_fk FOREIGN KEY (warehouse_id) REFERENCES warehouse_ref(id);


--
-- TOC entry 3523 (class 2606 OID 75423)
-- Name: role_permission_permission_right_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY role_permission
    ADD CONSTRAINT role_permission_permission_right_fk FOREIGN KEY (permission_id) REFERENCES permission_right(id);


--
-- TOC entry 3524 (class 2606 OID 75428)
-- Name: role_permission_role_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY role_permission
    ADD CONSTRAINT role_permission_role_fk FOREIGN KEY (role_id) REFERENCES role(id);


--
-- TOC entry 3525 (class 2606 OID 75433)
-- Name: single_window_operator_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY single_window_operator_ref
    ADD CONSTRAINT single_window_operator_organization_fk FOREIGN KEY (id) REFERENCES organization(id);


--
-- TOC entry 3522 (class 2606 OID 75418)
-- Name: target_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY risk_list
    ADD CONSTRAINT target_organization_fk FOREIGN KEY (target_id) REFERENCES organization(id);


--
-- TOC entry 3528 (class 2606 OID 75448)
-- Name: uploaded_document_document_setup_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY uploaded_document
    ADD CONSTRAINT uploaded_document_document_setup_fk FOREIGN KEY (document_setup_id) REFERENCES document_setup(id);


--
-- TOC entry 3527 (class 2606 OID 75443)
-- Name: uploaded_document_request_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY uploaded_document
    ADD CONSTRAINT uploaded_document_request_fk FOREIGN KEY (request_id) REFERENCES request(id);


--
-- TOC entry 3529 (class 2606 OID 75598)
-- Name: uploaded_document_vehicle_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY uploaded_document
    ADD CONSTRAINT uploaded_document_vehicle_fk FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);


--
-- TOC entry 3526 (class 2606 OID 75438)
-- Name: uploaded_document_vehicle_form_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY uploaded_document
    ADD CONSTRAINT uploaded_document_vehicle_form_fk FOREIGN KEY (vehicle_form_id) REFERENCES vehicle_form(id);


--
-- TOC entry 3552 (class 2606 OID 77469)
-- Name: user_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY message
    ADD CONSTRAINT user_fk FOREIGN KEY (user_id) REFERENCES member(id);


--
-- TOC entry 3531 (class 2606 OID 75458)
-- Name: user_role_member_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_member_fk FOREIGN KEY (member_id) REFERENCES member(id);


--
-- TOC entry 3530 (class 2606 OID 75453)
-- Name: user_role_role_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_role_fk FOREIGN KEY (role_id) REFERENCES role(id);


--
-- TOC entry 3535 (class 2606 OID 75478)
-- Name: vehicle_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_country_fk FOREIGN KEY (destination_id) REFERENCES country_ref(id);


--
-- TOC entry 3533 (class 2606 OID 75468)
-- Name: vehicle_exit_border_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_exit_border_fk FOREIGN KEY (exit_border_id) REFERENCES customs_office_ref(id);


--
-- TOC entry 3537 (class 2606 OID 75488)
-- Name: vehicle_form_buyer_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle_form
    ADD CONSTRAINT vehicle_form_buyer_fk FOREIGN KEY (buyer_id) REFERENCES vehicle_operator(id);


--
-- TOC entry 3538 (class 2606 OID 75493)
-- Name: vehicle_form_clearing_agent_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle_form
    ADD CONSTRAINT vehicle_form_clearing_agent_fk FOREIGN KEY (clearing_agent_id) REFERENCES economic_operator(id);


--
-- TOC entry 3536 (class 2606 OID 75483)
-- Name: vehicle_form_forwarding_agent_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle_form
    ADD CONSTRAINT vehicle_form_forwarding_agent_fk FOREIGN KEY (forwarding_agent_id) REFERENCES vehicle_operator(id);


--
-- TOC entry 3539 (class 2606 OID 75498)
-- Name: vehicle_form_user_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle_form
    ADD CONSTRAINT vehicle_form_user_fk FOREIGN KEY (user_id) REFERENCES member(id);


--
-- TOC entry 3540 (class 2606 OID 75503)
-- Name: vehicle_information_product_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle_information
    ADD CONSTRAINT vehicle_information_product_fk FOREIGN KEY (product_id) REFERENCES product(id);


--
-- TOC entry 3541 (class 2606 OID 75508)
-- Name: vehicle_operator_country_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle_operator
    ADD CONSTRAINT vehicle_operator_country_fk FOREIGN KEY (country_id) REFERENCES country_ref(id);


--
-- TOC entry 3542 (class 2606 OID 75513)
-- Name: vehicle_operator_organization_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle_operator
    ADD CONSTRAINT vehicle_operator_organization_fk FOREIGN KEY (visibility_organization_id) REFERENCES organization(id);


--
-- TOC entry 3532 (class 2606 OID 75463)
-- Name: vehicle_parc_vente_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_parc_vente_fk FOREIGN KEY (parc_vente_id) REFERENCES parc_vente_ref(id);


--
-- TOC entry 3534 (class 2606 OID 75473)
-- Name: vehicle_vehicle_form_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_vehicle_form_fk FOREIGN KEY (vehicle_form_id) REFERENCES vehicle_form(id);


--
-- TOC entry 3544 (class 2606 OID 75583)
-- Name: warehouse_lang_currency_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY warehouse_lang
    ADD CONSTRAINT warehouse_lang_currency_fk FOREIGN KEY (warehouse_id) REFERENCES warehouse_ref(id);


--
-- TOC entry 3543 (class 2606 OID 75578)
-- Name: warehouse_lang_lang_fk; Type: FK CONSTRAINT; Schema: epermits; Owner: epermits
--

ALTER TABLE ONLY warehouse_lang
    ADD CONSTRAINT warehouse_lang_lang_fk FOREIGN KEY (lang_id) REFERENCES lang(id);


-- Completed on 2015-11-28 16:15:53

--
-- PostgreSQL database dump complete
--

