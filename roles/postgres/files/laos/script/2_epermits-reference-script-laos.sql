-- Packaging

/*
DROP FOREIGN TABLE IF EXISTS epermits.packaging_ref_csv;

CREATE FOREIGN TABLE epermits.packaging_ref_csv (
	code text,
	libelle_lao text,
	libelle_eng text	
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-packaging-laos.csv', format 'csv', delimiter ';' , header 'TRUE');


insert into epermits.packaging_ref(id, code) select nextval('epermits.ref_data_seq'), code from epermits.packaging_ref_csv order by code;

insert into epermits.packaging_lang(id, lang_id, packaging_id, label) select nextval('epermits.ref_data_seq'), l.id, pr.id, prc.libelle_lao from epermits.lang l, epermits.packaging_ref pr, epermits.packaging_ref_csv prc
	where l.code='lo' and pr.code=prc.code;


insert into epermits.packaging_lang(id, lang_id, packaging_id, label) select nextval('epermits.ref_data_seq'), l.id, pr.id, prc.libelle_eng from epermits.lang l, epermits.packaging_ref pr, epermits.packaging_ref_csv prc
	where l.code='en' and pr.code=prc.code;
*/

-- Currencies

 DROP FOREIGN TABLE IF EXISTS epermits.currency_ref_csv;

CREATE FOREIGN TABLE epermits.currency_ref_csv (
	code text,
	libelle_eng text,
	libelle text	
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-currencies-laos.csv', format 'csv', delimiter ';' , header 'TRUE');

-- Countries

 DROP FOREIGN TABLE IF EXISTS epermits.country_ref_csv;

CREATE FOREIGN TABLE epermits.country_ref_csv (
	iso_num text,
	iso_alpha_3 text,
	iso_alpha_2 text,
	label_en text,
	label_lao text,
	economic_zone text,
	geographics_zone text,
	geographic_zone_lao text
) SERVER ref_data 
OPTIONS ( filename '/appli/epermits/database/data/ref-countries-laos.csv', format 'csv', delimiter ';' , header 'TRUE');

	

-- Ports

 DROP FOREIGN TABLE IF EXISTS epermits.ports_ref_csv;

CREATE FOREIGN TABLE epermits.ports_ref_csv (
	code text,
	label text,
	label_eng text,
	country text NOT NULL
	
) SERVER ref_data 
OPTIONS ( filename '/appli/epermits/database/data/ref-loading-ports-and-airports-laos.csv', format 'csv', delimiter ';', header 'TRUE' );




 DROP FOREIGN TABLE IF EXISTS epermits.customs_office_ref_csv;

CREATE FOREIGN TABLE epermits.customs_office_ref_csv (
	code text,
	libelle text,
	lib_eng text
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-customs-offices-laos.csv', format 'csv', delimiter ';', header 'TRUE');


-- Customs Regim

 DROP FOREIGN TABLE IF EXISTS epermits.customs_regim_ref_csv;

CREATE FOREIGN TABLE epermits.customs_regim_ref_csv (
	code text,
	label_lao text,
	label_en text,
	isimport text,
	isexport text,
	istransit text
) SERVER ref_data 
OPTIONS ( filename '/appli/epermits/database/data/ref-declaration-types-laos.csv', format 'csv', delimiter ';', header 'TRUE' );


-- Document Types
DROP FOREIGN TABLE IF EXISTS epermits.document_type_ref_csv;


CREATE FOREIGN TABLE epermits.document_type_ref_csv (
	code text,
	libelle text,
	lib_eng text
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-document-types-laos.csv', format 'csv', delimiter ';', header 'TRUE');



DROP FOREIGN TABLE IF EXISTS epermits.goods_location_ref_csv;


CREATE FOREIGN TABLE epermits.goods_location_ref_csv (
	code text,
	libelle text,
	lib_eng text
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-goods-locations-laos.csv', format 'csv', delimiter ';', header 'TRUE');


 DROP FOREIGN TABLE IF EXISTS epermits.warehouse_ref_csv;


CREATE FOREIGN TABLE epermits.warehouse_ref_csv (
	code text,
	libelle text,
	lib_eng text
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-warehouses-laos.csv', format 'csv', delimiter ';', header 'TRUE');


-- Extended Procedure Code
 DROP FOREIGN TABLE IF EXISTS epermits.extended_procedure_code_ref_csv;

CREATE FOREIGN TABLE epermits.extended_procedure_code_ref_csv (
	code text,
	libelle text,
	type text,
	lib_eng text
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-customs-procedures-laos.csv', format 'csv', delimiter ';', header 'TRUE');



-- National Procedure Code

DROP FOREIGN TABLE IF EXISTS epermits.national_procedure_code_ref_csv;

CREATE FOREIGN TABLE epermits.national_procedure_code_ref_csv (
	code text,
	libelle text,
	type text,
	lib_eng text
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-additional-codes-laos.csv', format 'csv', delimiter ';', header 'TRUE');

-- PACKAGING

DROP FOREIGN TABLE IF EXISTS epermits.packaging_ref_csv;

CREATE FOREIGN TABLE epermits.packaging_ref_csv (
	code text,
	libelle_lo text,
	libelle_en text
) SERVER ref_data
OPTIONS ( filename '/appli/epermits/database/data/ref-type-of-packages-laos.csv', format 'csv', delimiter ';' , header 'TRUE');


--- Lang
INSERT INTO epermits.LANG ("id", "code", "name")  select nextval('epermits.ref_data_seq'), 'fr-bj', 'Français';
INSERT INTO epermits.LANG ("id", "code", "name")  select nextval('epermits.ref_data_seq'), 'en', 'English';
INSERT INTO epermits.LANG ("id", "code", "name")  select nextval('epermits.ref_data_seq'), 'lo', 'Lao';

-- CustomsRegimeRef

INSERT INTO epermits.customs_regim_ref(id, code,regim) 
	select nextval('epermits.ref_data_seq'), code, case when isimport='X' THEN 'IMPORT' WHEN isexport='X' THEN 'EXPORT' ELSE 'TRANSIT' END 
	from epermits.customs_regim_ref_csv order by code;

insert into epermits.customs_regim_lang(id, lang_id, customs_regim_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.label_lao 
	from epermits.lang l, epermits.customs_regim_ref cr, epermits.customs_regim_ref_csv crc
	where l.code='lo' and cr.code=crc.code;

insert into epermits.customs_regim_lang(id, lang_id, customs_regim_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.label_en 
	from epermits.lang l, epermits.customs_regim_ref cr, epermits.customs_regim_ref_csv crc
	where l.code='en' and cr.code=crc.code;


-- Transportation Type

-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Maritime transport', 'ການຂົນສົ່ງທາງນໍ້າ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Rail transport', 'ການຂົນສົ່ງທາງລົດໄຟ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Road transport', 'ການຂົນສົ່ງທາງບົກ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Air transport', 'ການຂົນສົ່ງທາງອາກາດ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Mail', 'ພັດສະດຸ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Multimodal transport', 'ຮຸບແບບການຂົນສົ່ງທີ່ຫຼາກຫຼາຍ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Fixed transport installation', 'ການຕິດຕັ້ງການຂົນສົ່ງທີ່ແນ່ນອນ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Inland water transport', 'ການຂົນສົ່ງທາງນໍ້າພາຍໃນປະເທດ');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Transport mode not applicable', 'ຮຸູບແບບການຂົນສົ່ງທີ່ບໍ່ສາມາດນໍາໃຊ້ໄດ້');
-- INSERT INTO epermits.transportation_type_ref(id, label_en, label) VALUES (nextval('epermits.ref_data_seq'), 'Transport mode not specified / Transport mode has not been specified', 'ຮູບແບບການຂົນສົ່ງບໍ່ຊັດເຈນ / ຮູບແບບການຂົນສົ່ງຍັງບໍໄດ້ລະບຸ');


-- CSV imports

insert into epermits.currency_ref(id, code) select nextval('epermits.ref_data_seq'), code from epermits.currency_ref_csv order by code;
insert into epermits.currency_lang(id, lang_id, currency_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.libelle from epermits.lang l, epermits.currency_ref cr, epermits.currency_ref_csv crc
	where l.code='lo' and cr.code=crc.code;
insert into epermits.currency_lang(id, lang_id, currency_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.libelle_eng from epermits.lang l, epermits.currency_ref cr, epermits.currency_ref_csv crc
	where l.code='en' and cr.code=crc.code;
	

-- Country_ref

insert into epermits.country_ref(id, code, code_court) select nextval('epermits.ref_data_seq'), iso_alpha_3, iso_alpha_2 from epermits.country_ref_csv;
insert into epermits.country_lang(id, lang_id, country_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.label_lao from epermits.lang l, epermits.country_ref cr, epermits.country_ref_csv crc
	where l.code='lo' and cr.code=crc.iso_alpha_3;
insert into epermits.country_lang(id, lang_id, country_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.label_en from epermits.lang l, epermits.country_ref cr, epermits.country_ref_csv crc
	where l.code='en' and cr.code=crc.iso_alpha_3;


-- Port_ref

insert into epermits.port_ref(id, code, active, country_id) 
	select nextval('epermits.ref_data_seq'), p.code, true, c.id 
	from epermits.ports_ref_csv p, epermits.country_ref c 
	where c.code=p.country;
	
	
insert into epermits.port_lang(id, lang_id, port_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, pr.id, crc.label_eng
	from epermits.lang l, epermits.port_ref pr, epermits.ports_ref_csv crc, epermits.country_ref cr
	where l.code='en' and pr.code=crc.code and pr.country_id=cr.id and crc.country=cr.code;


insert into epermits.port_lang(id, lang_id, port_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, pr.id, crc.label 
	from epermits.lang l, epermits.port_ref pr, epermits.ports_ref_csv crc, epermits.country_ref cr
	where l.code='lo' and pr.code=crc.code and pr.country_id=cr.id and crc.country=cr.code;
	

-- Customs_office_Ref


insert into epermits.customs_office_ref(id, code) select nextval('epermits.ref_data_seq'), code from epermits.customs_office_ref_csv;
insert into epermits.customs_office_lang(id, lang_id, customs_Office_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.libelle from epermits.lang l, epermits.customs_office_ref cr, epermits.customs_office_ref_csv crc
	where l.code='lo' and cr.code=crc.code;
insert into epermits.customs_office_lang(id, lang_id, customs_Office_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.lib_eng from epermits.lang l, epermits.customs_office_ref cr, epermits.customs_office_ref_csv crc
	where l.code='en' and cr.code=crc.code;

-- Goods_locations_ref

insert into epermits.goods_location_ref(id, code) select nextval('epermits.ref_data_seq'), p.code from epermits.goods_location_ref_csv p;
insert into epermits.goods_location_lang(id, lang_id, goods_location_ref_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.libelle from epermits.lang l, epermits.goods_location_ref cr, epermits.goods_location_ref_csv crc
	where l.code='lo' and cr.code=crc.code;
insert into epermits.goods_location_lang(id, lang_id, goods_location_ref_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.lib_eng from epermits.lang l, epermits.goods_location_ref cr, epermits.goods_location_ref_csv crc
	where l.code='en' and cr.code=crc.code;


-- Warehouses

INSERT INTO epermits.warehouse_ref(id, code) select nextval('epermits.ref_data_seq'), code from epermits.warehouse_ref_csv;
insert into epermits.warehouse_lang(id, lang_id, warehouse_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.libelle from epermits.lang l, epermits.warehouse_ref cr, epermits.warehouse_ref_csv crc
	where l.code='lo' and cr.code=crc.code;
insert into epermits.warehouse_lang(id, lang_id, warehouse_id, label) select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.lib_eng from epermits.lang l, epermits.warehouse_ref cr, epermits.warehouse_ref_csv crc
	where l.code='en' and cr.code=crc.code;

-- Extended Procedure Code

INSERT INTO epermits.extended_procedure_code_ref(id, code, custom_regime_id) 
	select nextval('epermits.ref_data_seq'), b.code, c.id 
	from epermits.extended_procedure_code_ref_csv b, epermits.customs_regim_ref c 
	where (position(c.code in b.type)>0 or b.type='*');

insert into epermits.extended_procedure_code_lang(id, lang_id, extended_procedure_code_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.libelle 
	from epermits.lang l, epermits.extended_procedure_code_ref cr, epermits.extended_procedure_code_ref_csv crc
	where l.code='lo' and cr.code=crc.code;

insert into epermits.extended_procedure_code_lang(id, lang_id, extended_procedure_code_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.lib_eng 
	from epermits.lang l, epermits.extended_procedure_code_ref cr, epermits.extended_procedure_code_ref_csv crc
	where l.code='en' and cr.code=crc.code;

-- National Procedure Code

INSERT INTO epermits.national_procedure_code_ref(id, code, extended_procedure_code_id) 
	select nextval('epermits.ref_data_seq'), b.code, c.id 
	from epermits.national_procedure_code_ref_csv b, epermits.extended_procedure_code_ref c where (position(c.code in b.type)>0 or b.type='*');

insert into epermits.national_procedure_code_lang(id, lang_id, national_procedure_code_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.libelle 
	from epermits.lang l, epermits.national_procedure_code_ref cr, epermits.national_procedure_code_ref_csv crc
	where l.code='lo' and cr.code=crc.code;

insert into epermits.national_procedure_code_lang(id, lang_id, national_procedure_code_id, label) 
	select nextval('epermits.ref_data_seq'), l.id, cr.id, crc.lib_eng 
	from epermits.lang l, epermits.national_procedure_code_ref cr, epermits.national_procedure_code_ref_csv crc
	where l.code='en' and cr.code=crc.code;


INSERT INTO epermits.document_type_ref(id, code, label) 
	SELECT nextval('epermits.ref_data_seq'), b.code, b.libelle 
	FROM epermits.document_type_ref_csv b;




insert into epermits.packaging_ref(id, code) select nextval('epermits.ref_data_seq'), code from epermits.packaging_ref_csv order by code;

insert into epermits.packaging_lang(id, lang_id, packaging_id, label) select nextval('epermits.ref_data_seq'), l.id, pr.id, prc.libelle_lo from epermits.lang l, epermits.packaging_ref pr, epermits.packaging_ref_csv prc
	where l.code='lo' and pr.code=prc.code;
	
insert into epermits.packaging_lang(id, lang_id, packaging_id, label) select nextval('epermits.ref_data_seq'), l.id, pr.id, prc.libelle_en from epermits.lang l, epermits.packaging_ref pr, epermits.packaging_ref_csv prc
	where l.code='en' and pr.code=prc.code;



DROP FOREIGN TABLE IF EXISTS epermits.currency_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.country_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.ports_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.customs_office_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.document_type_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.goods_location_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.warehouse_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.extended_procedure_code_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.national_procedure_code_ref_csv;

DROP FOREIGN TABLE IF EXISTS epermits.packaging_ref_csv;

----------------------------------
------ Roles ---------------------
----------------------------------	
INSERT INTO epermits.role(id, name, created_by, updated_by, created_on, updated_on, description, rank) select nextval('epermits.ref_data_seq'), 'CLEARING_AGENT', 'BV_HO_ADMIN', 'BV_HO_ADMIN', '2014-04-27', '2014-04-27', 'CLEARING_AGENT role', 300;
INSERT INTO epermits.role(id, name, created_by, updated_by, created_on, updated_on, description, rank) select nextval('epermits.ref_data_seq'), 'DG_DEPARTMENT', 'BV_HO_ADMIN', 'BV_HO_ADMIN', '2014-04-27', '2014-04-27', 'DG_DEPARTMENT role', 200;
INSERT INTO epermits.role(id, name, created_by, updated_by, created_on, updated_on, description, rank) select nextval('epermits.ref_data_seq'), 'CUSTOMS_OFFICER', 'BV_HO_ADMIN', 'BV_HO_ADMIN', '2014-04-27', '2014-04-27', 'CUSTOMS_OFFICER role', 300;
INSERT INTO epermits.role(id, name, created_by, updated_by, created_on, updated_on, description, rank) select nextval('epermits.ref_data_seq'), 'CASHIER', 'BV_HO_ADMIN', 'BV_HO_ADMIN', '2014-04-27', '2014-04-27', 'CASHIER role', 200;
INSERT INTO epermits.role(id, name, created_by, updated_by, created_on, updated_on, description, rank) select nextval('epermits.ref_data_seq'), 'BV_LOCAL_USER_SUPPORT', 'BV_HO_ADMIN', 'BV_HO_ADMIN', '2014-04-27', '2014-04-27', 'BV_LOCAL_USER_SUPPORT role', 300;
INSERT INTO epermits.role(id, name, created_by, updated_by, created_on, updated_on, description, rank) select nextval('epermits.ref_data_seq'), 'BV_LOCAL_ADMIN', 'BV_HO_ADMIN', 'BV_HO_ADMIN', '2014-04-27', '2014-04-27', 'BV_LOCAL_ADMIN role', 200;
INSERT INTO epermits.role(id, name, created_by, updated_by, created_on, updated_on, description, rank) select nextval('epermits.ref_data_seq'), 'BV_HO_ADMIN', 'BV_HO_ADMIN', 'BV_HO_ADMIN', '2015-09-18', '2015-09-18', 'BV_HO_ADMIN role', 100;
INSERT INTO epermits.role (id, name, created_by, updated_by, created_on, updated_on, description, rank) SELECT nextval('epermits.ref_data_seq'), 'HEAD_OF_DIVISION', 'BV_LOCAL_ADMIN', 'BV_LOCAL_ADMIN', '2015-11-18', '2015-11-18', 'HEAD_OF_DIVISION role', 300;
INSERT INTO epermits.role (id, name, created_by, updated_by, created_on, updated_on, description, rank) SELECT nextval('epermits.ref_data_seq'), 'INVOICING_CASHIER', 'BV_LOCAL_ADMIN', 'BV_LOCAL_ADMIN', '2015-11-18', '2015-11-18', 'INVOICING_CASHIER role', 300;
INSERT INTO epermits.role (id, name, created_by, updated_by, created_on, updated_on, description, rank) SELECT nextval('epermits.ref_data_seq'), 'TECHNICAL_OFFICER', 'BV_LOCAL_ADMIN', 'BV_LOCAL_ADMIN', '2015-11-18', '2015-11-18', 'TECHNICAL_OFFICER role', 300;
