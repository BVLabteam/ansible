#!/bin/expect
set timeout -1


spawn /tmp/alfresco-community-5.0.d-installer-linux-x64.bin

expect "Please choose an option"
send   "2\n"
expect  "Merci de choisi une option."
send    "1\n"
expect  "Sélectionner un dossier "
send    "/appli/alfresco/\n"
expect  "Mot de passe admin"
send    "Daj7T6h3\n"
expect  "Répéter le mot de passe :"
send    "Daj7T6h3\n"
expect  "Installer Alfresco Community en tant que service ?"
send    "y\n"
expect  "Voulez-vous continuer ?"
send     "y\n"
expect  "Nom du script de service"
send    "alfresco\n"
expect  "Voir le fichier Lisezmoi"
send    "n\n"
#expect  "Service script name"
#send    "alfresco\n"
#expect  "Pressez [Entrée] pour continuer"
#send    "\r";