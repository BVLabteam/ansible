#!/bin/bash
if test $# -eq 0
then
  echo "NEED 3 UNNAMED ARGUMENTS : DBNAME, TABLENAME AND COLUMNNAME"
  exit 3
fi

YESTERDAY=`date '+%Y-%m-%d' --date '1 days ago'`

DATABASE=$1
TABLE=$2
COLUMN=$3

RESULT=`mysql -udb_user -p2store4GOOD -e "SELECT $COLUMN FROM $DATABASE.$TABLE" | grep -v $COLUMN`

if [[ $YESTERDAY == $RESULT ]]
then
  echo LAST $COLUMN : $RESULT
  exit 0
else
  echo "LAST $COLUMN : $RESULT (EXPECTED : $YESTERDAY)"
  exit 2
fi
